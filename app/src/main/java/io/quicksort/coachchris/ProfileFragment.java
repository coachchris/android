package io.quicksort.coachchris;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.Date;

import io.quicksort.coachchris.backend.AccountService;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.models.Activity;
import io.quicksort.coachchris.models.OwnedProgram;
import io.quicksort.coachchris.utils.Photo;
import io.quicksort.coachchris.utils.Session;
import io.quicksort.coachchris.views.HorizontalImageTextViewHolder;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmModel;

public class ProfileFragment extends Fragment {

    private static final String ARG_ACCOUNT_KEY = "accountKey";

    private LocalBroadcastManager mLbm;
    private Realm mRealm;
    private RecyclerView mActivityList;
    private Account mAccount;
    private ImageView mProfilePhoto;
    private Account mCurrentUser;

    /**
     * Performs actions once the viewed account has been fetched from api and saved to local db
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            View v = getView();
            if (v == null) {
                return;
            }

            String msg = intent.getStringExtra(AccountService.BROADCAST_MESSAGE);
            if (msg.length() > 0) {
                Snackbar.make(v, msg, Snackbar.LENGTH_LONG).show();
            }
        }
    };

    /**
     * Static helper method to package up the required fragment data
     *
     * @param accountKey account key of the account being viewed
     * @return ProfileFragment object
     */
    public static ProfileFragment newInstance(String accountKey) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ACCOUNT_KEY, accountKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();
        mCurrentUser = Session.getCurrentUser(getContext(), mRealm);
        mLbm = LocalBroadcastManager.getInstance(getContext());
        mLbm.registerReceiver(mMessageReceiver, new IntentFilter(AccountService.ACTION_FETCH_ACCOUNT));
        mLbm.registerReceiver(mMessageReceiver, new IntentFilter(AccountService.ACTION_FOLLOW));
        mLbm.registerReceiver(mMessageReceiver, new IntentFilter(AccountService.ACTION_UNFOLLOW));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mCurrentUser.removeChangeListeners();
        mAccount.removeChangeListeners();
        mLbm.unregisterReceiver(mMessageReceiver);

        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        // ui
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.profile_toolbar);
        mProfilePhoto = (ImageView) v.findViewById(R.id.profile_photo);

        // toolbar
        AppCompatActivity a = (AppCompatActivity) getActivity();
        a.setSupportActionBar(toolbar);
        ActionBar actionBar = a.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // activities
        mActivityList = (RecyclerView) v.findViewById(android.R.id.list);
        mActivityList.setLayoutManager(new LinearLayoutManager(getContext()));

        // bind account
        String accountKey = getArguments().getString(ARG_ACCOUNT_KEY);
        mAccount = mRealm.where(Account.class).equalTo(Account.ATTR_KEY, accountKey).findFirstAsync();
        mAccount.addChangeListener(new RealmChangeListener<RealmModel>() {
            @Override
            public void onChange(RealmModel model) {
                if (!mAccount.isLoaded() || !mAccount.isValid()) {
                    return;
                }

                // bind data
                ProfileAdapter adapter = new ProfileAdapter(getContext(), mAccount);
                mActivityList.setAdapter(adapter);

                // bind ui
                CollapsingToolbarLayout ctl = (CollapsingToolbarLayout) getView().findViewById(R.id.collapsing_toolbar_layout);
                ctl.setTitle(mAccount.name);

                int photoSize = (int) getResources().getDimension(R.dimen.profile_image_width);
                String photoUrl = Photo.getSizedUrl(getContext(), mAccount.photo.name, photoSize, 0);
                Picasso.with(getContext()).load(photoUrl).into(mProfilePhoto);
            }
        });
        AccountService.fetchAccount(getContext(), accountKey);

        return v;
    }

    /**
     * Handle follow/unfollow button click event within list header
     */
    public void toggleFollowingStatus() {
        if (mCurrentUser.isFollowing(mAccount)) {
            AccountService.unfollowAccount(getContext(), mAccount.key);
        } else {
            AccountService.followAccount(getContext(), mAccount.key);
        }
    }

    /**
     * Adapter to peform binding on the list of activity items for the account being viewed
     */
    private class ProfileAdapter extends GroupedRecyclerViewAdapter {

        private static final int TYPE_PROFILE_ACTIONS = 0;
        private static final int TYPE_PROGRAM_HEADER = 1;
        private static final int TYPE_PROGRAM = 2;
        private static final int TYPE_ACTIVITY_HEADER = 3;
        private static final int TYPE_ACTIVITY = 4;

        private final long mNow;
        private CharSequence[] mActions;

        private RealmList<OwnedProgram> mPrograms;
        private RealmList<Activity> mActivity;

        private int avatarWidth;

        ProfileAdapter(@NonNull Context context, Account account) {
            mActions = context.getResources().getTextArray(R.array.user_activity_actions);
            mNow = new Date().getTime();

            mPrograms = account.ownedPrograms;
            mActivity = account.activity;

            addGroup(TYPE_PROFILE_ACTIONS, 1);
            if (mPrograms.size() > 0) {
                addGroup(TYPE_PROGRAM_HEADER, 1);
                addGroup(TYPE_PROGRAM, mPrograms.size());
            }

            if (mActivity.size() > 0) {
                addGroup(TYPE_ACTIVITY_HEADER, 1);
                addGroup(TYPE_ACTIVITY, mActivity.size());
            }

            avatarWidth = (int) context.getResources().getDimension(R.dimen.list_avatar_diameter);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v;
            LayoutInflater inflater = LayoutInflater.from(getContext());
            switch (viewType) {
                case TYPE_PROFILE_ACTIONS:
                    v = inflater.inflate(R.layout.profile_header, parent, false);
                    return new ProfileHeaderViewHolder(v);
                case TYPE_PROGRAM_HEADER:
                    v = ListHeaderHolder.getView(inflater, parent);
                    return new ListHeaderHolder(v);
                case TYPE_PROGRAM:
                    v = HorizontalImageTextViewHolder.getView(inflater, parent);
                    return new HorizontalImageTextViewHolder(v);
                case TYPE_ACTIVITY_HEADER:
                    v = ListHeaderHolder.getView(inflater, parent);
                    return new ListHeaderHolder(v);
                case TYPE_ACTIVITY:
                    v = inflater.inflate(R.layout.activity_list_item, parent, false);
                    return new ActivityViewHolder(v);
                default:
                    throw new RuntimeException("Invalid ProfileAdapter viewType");
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            int groupedPosition = getRelativeIndex(position);

            switch (getItemViewType(position)) {
                case TYPE_PROGRAM_HEADER:
                    bindProgramHeader(holder);
                    break;
                case TYPE_PROGRAM:
                    bindProgram(holder, groupedPosition);
                    break;
                case TYPE_ACTIVITY_HEADER:
                    bindActivityHeader(holder);
                    break;
                case TYPE_ACTIVITY:
                    bindActivity(holder, groupedPosition);
                    break;
            }
        }

        private void bindActivityHeader(RecyclerView.ViewHolder holder) {
            ListHeaderHolder h = (ListHeaderHolder) holder;
            h.setHeader(R.string.profile_list_header_activities);
        }

        private void bindProgramHeader(RecyclerView.ViewHolder holder) {
            ListHeaderHolder h = (ListHeaderHolder) holder;
            h.setHeader(R.string.profile_list_header_programs);
        }

        private void bindProgram(RecyclerView.ViewHolder holder, int position) {
            HorizontalImageTextViewHolder h = (HorizontalImageTextViewHolder) holder;
            OwnedProgram p = mPrograms.get(position);
            String photoUrl = Photo.getSizedUrl(getContext(), p.photo.name, avatarWidth, 0);
            h.setImageDrawable(photoUrl);
            h.setText(p.name);
        }

        private void bindActivity(RecyclerView.ViewHolder h, int position) {
            ActivityViewHolder holder = (ActivityViewHolder) h;

            Activity a = mActivity.get(position);
            if (a == null) {
                return;
            }

            CharSequence template = mActions[a.code];

            // timestamp
            long timestamp = a.timestamp.getTime();
            CharSequence relTime = DateUtils.getRelativeTimeSpanString(timestamp, mNow, DateUtils.DAY_IN_MILLIS);
            holder.Timestamp.setText(relTime);

            // action
            String name = a.link == null ? a.account.name : a.link.name;
            holder.Action.setText(Html.fromHtml(String.format(template.toString(), name)));

            // image - show linked data picture or a drawable with the first letter of the linked data

            String pictureName = (a.link == null) ? a.account.photo.name : a.link.photo.name;
            if (!TextUtils.isEmpty(pictureName)) {
                String photoUrl = Photo.getSizedUrl(getContext(), pictureName, avatarWidth, 0);
                Picasso.with(getContext()).load(photoUrl).into(holder.ProfilePhoto);
            } else {
                ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
                int color = generator.getColor(name);
                TextDrawable drawable = TextDrawable.builder().buildRect(String.valueOf(name.charAt(0)), color);
                holder.ProfilePhoto.setImageDrawable(drawable);
            }
        }

        /**
         * Binds the first item in the activity list that acts as a header
         */
        class ProfileHeaderViewHolder extends RecyclerView.ViewHolder {
            private final Button btnFollow;
            private final TextView currentProgram;

            ProfileHeaderViewHolder(View v) {
                super(v);

                btnFollow = (Button) v.findViewById(R.id.profile_follow);
                currentProgram = (TextView) v.findViewById(R.id.profile_current_program);

                // button
                if (mAccount.key.equals(mCurrentUser.key)) {
                   btnFollow.setVisibility(View.GONE);
                } else {
                    updateButtonText();
                    mCurrentUser.addChangeListener(new RealmChangeListener<RealmModel>() {
                        @Override
                        public void onChange(RealmModel element) {
                            updateButtonText();
                        }
                    });

                    btnFollow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toggleFollowingStatus();
                        }
                    });
                }

                // bind account details
                if (mAccount.inProgram()) {
                    String p = getResources().getString(R.string.profile_in_the_program, mAccount.program.name);
                    currentProgram.setText(Html.fromHtml(p));
                } else {
                    currentProgram.setText(R.string.profile_not_in_program);
                }
            }

            private void updateButtonText() {
                if (mCurrentUser.isFollowing(mAccount)) {
                    btnFollow.setText(R.string.friend_unfollow);
                } else {
                    btnFollow.setText(R.string.friend_follow);
                }
            }
        }

        /**
         * Performs the databinding for each of the activity list items
         */
        class ActivityViewHolder extends RecyclerView.ViewHolder {

            private CircularImageView ProfilePhoto;
            private TextView Action;
            private TextView Timestamp;

            ActivityViewHolder(View v) {
                super(v);
                ProfilePhoto = (CircularImageView) v.findViewById(R.id.activity_profile_photo);
                Action = (TextView) v.findViewById(R.id.activity_action);
                Timestamp = (TextView) v.findViewById(R.id.activity_timestamp);
            }
        }
    }

}
