package io.quicksort.coachchris.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.quicksort.coachchris.ProgramDetailsActivity;
import io.quicksort.coachchris.R;
import io.quicksort.coachchris.models.Program;
import io.quicksort.coachchris.utils.Photo;
import io.quicksort.coachchris.utils.Placeholder;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class ProgramsAdapter extends RealmRecyclerViewAdapter<Program, ProgramsAdapter.ViewHolder> {

    int avatarWidth;

    public ProgramsAdapter(Context context, @Nullable OrderedRealmCollection<Program> data) {
        super(context, data, true);
        avatarWidth = (int) context.getResources().getDimension(R.dimen.list_avatar_diameter);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.program_list_item, parent, false);
        return new ProgramsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Program p = getItem(position);
        if (p == null) {
            return;
        }

        holder.programKey = p.key;
        holder.name.setText(p.name);
        holder.details.setText(p.description);

        if (!TextUtils.isEmpty(p.photo.name)) {
            String url = Photo.getSizedUrl(context, p.photo.name, avatarWidth, 0);
            Picasso.with(context)
                    .load(url)
                    .placeholder(Placeholder.getDrawable(p.name))
                    .into(holder.img);
        } else {
            holder.img.setImageDrawable(Placeholder.getDrawable(p.name));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView name;
        TextView details;
        String programKey;

        ViewHolder(View v) {
            super(v);

            name = (TextView) v.findViewById(R.id.program_name);
            details = (TextView) v.findViewById(R.id.program_details);
            img = (ImageView) v.findViewById(R.id.program_photo);

            CardView card = (CardView) v.findViewById(R.id.program_card_view);
            card.setPreventCornerOverlap(false);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = ProgramDetailsActivity.getIntent(context, programKey);
                    context.startActivity(intent);
                }
            });
        }
    }
}
