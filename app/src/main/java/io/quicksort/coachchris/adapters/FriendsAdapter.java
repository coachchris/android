package io.quicksort.coachchris.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import io.quicksort.coachchris.ProfileActivity;
import io.quicksort.coachchris.R;
import io.quicksort.coachchris.models.Person;
import io.quicksort.coachchris.utils.Photo;
import io.quicksort.coachchris.utils.Placeholder;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class FriendsAdapter extends RealmRecyclerViewAdapter<Person, FriendsAdapter.ViewHolder> {

    private final int avatarWidth;

    public FriendsAdapter(Context c, @Nullable OrderedRealmCollection<Person> data) {
        super(c, data, false);

        avatarWidth = (int) context.getResources().getDimension(R.dimen.list_avatar_diameter);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.friend_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            @SuppressWarnings("ConstantConditions")
            Person person = getData().get(position);
            holder.accountKey = person.key;
            holder.userName.setText(person.name);
            String url = Photo.getSizedUrl(context, person.photo.name, avatarWidth, 0);
            Picasso.with(context)
                    .load(url)
                    .placeholder(Placeholder.getDrawable(person.name))
                    .into(holder.userPhoto);
        } catch (NullPointerException ex) {
            Toast.makeText(context, R.string.error_friend_binding, Toast.LENGTH_LONG).show();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        String accountKey;
        ImageView userPhoto;
        TextView userName;

        ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            userPhoto = (ImageView) v.findViewById(R.id.friend_picture);
            userName = (TextView) v.findViewById(R.id.friend_name);
        }

        @Override
        public void onClick(View view) {
            Intent intent = ProfileActivity.getIntent(context, accountKey);
            context.startActivity(intent);
        }
    }
}
