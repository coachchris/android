package io.quicksort.coachchris.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Workout extends RealmObject {
    public static final String ATTR_KEY = "key";

    @PrimaryKey
    public String key;

    // Attributes
    public String title;
    public String details;

    // Relations
    public RealmList<Exercise> exercises;
}
