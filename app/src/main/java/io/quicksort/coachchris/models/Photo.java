package io.quicksort.coachchris.models;

import io.realm.RealmObject;

public class Photo extends RealmObject {
    public String type;
    public String name;
}
