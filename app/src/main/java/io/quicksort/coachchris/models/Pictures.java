package io.quicksort.coachchris.models;

import io.realm.RealmObject;

public class Pictures extends RealmObject {
    public String small;
    public String medium;
    public String large;
}
