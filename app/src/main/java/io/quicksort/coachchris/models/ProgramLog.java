package io.quicksort.coachchris.models;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProgramLog extends RealmObject {
    public static final String ATTR_KEY = "key";

    @PrimaryKey
    public String key;

    public int dayIndex;
    public Date startedOn;
    public Date endedOn;
    public boolean completed;
}
