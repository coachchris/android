package io.quicksort.coachchris.models;

import io.realm.RealmObject;

public class Exercise extends RealmObject {
    public String details;
    public String videoId;
    public String videoPreviewUrl;
}
