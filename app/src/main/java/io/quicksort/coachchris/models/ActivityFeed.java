package io.quicksort.coachchris.models;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ActivityFeed extends RealmObject {
    public static final String ATTR_KEY = "key";

    @PrimaryKey
    public String key;

    public int code;
    public Date timestamp;

    public Relation account;
    public Relation link;
}
