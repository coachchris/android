package io.quicksort.coachchris.models;

import io.realm.RealmObject;

public class Relation extends RealmObject {
    public String key;
    public String name;
    public Photo photo;
}
