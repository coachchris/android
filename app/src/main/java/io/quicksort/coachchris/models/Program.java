package io.quicksort.coachchris.models;

import java.util.Date;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Program extends RealmObject {

    public static final String ATTR_KEY = "key";

    @PrimaryKey
    public String key;

    // Attributes
    public String name;
    public String description;
    public String instructions;
    public Date createdAt;
    public int level;
    public RealmList<StringValue> tags;
    public RealmList<StringValue> physicalRequirements;
    public RealmList<StringValue> equipmentRequirements;
    public Photo photo;

    public int totalParticipantCount;

    // Relations
    public RealmList<Person> knownParticipants;
    public RealmList<Workout> workouts;
}
