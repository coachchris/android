package io.quicksort.coachchris.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StringValue extends RealmObject {
    @PrimaryKey
    public String value;
}
