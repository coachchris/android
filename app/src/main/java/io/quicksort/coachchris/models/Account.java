package io.quicksort.coachchris.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Account extends RealmObject {
    public static final String ATTR_KEY = "key";
    public static final String ATTR_NAME = "name";

    @PrimaryKey
    public String key;

    public String authProviderId;
    public String authProviderName;
    public String gender;
    public String firstName;
    public String lastName;
    public String name;
    public Photo photo;

    // Relations
    public RealmList<OwnedProgram> ownedPrograms;
    public RealmList<Person> following;
    public RealmList<Activity> activity;
    public Program program;

    // only returned in the payload for the user's account
    public ProgramLog programLog;

    public boolean inProgram() {
        return program != null;
    }

    public boolean inProgram(Program p) {
        return program != null && program.key != null && program.key.equals(p.key);
    }

    public boolean isFollowing(Account account) {
        for (Person f : following) {
            if (f.key.equals(account.key)) {
                return true;
            }
        }
        return false;
    }
}
