package io.quicksort.coachchris.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Person extends RealmObject {
    public static final String ATTR_KEY = "key";

    @PrimaryKey
    public String key;

    public String name;
    public Photo photo;
}
