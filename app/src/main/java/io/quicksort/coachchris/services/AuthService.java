package io.quicksort.coachchris.services;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;

import io.quicksort.coachchris.R;
import io.quicksort.coachchris.backend.ApiContract;
import io.quicksort.coachchris.backend.BackendService;
import io.quicksort.coachchris.backend.Endpoints;
import io.quicksort.coachchris.utils.Session;
import retrofit2.Response;

public class AuthService extends BackendService {
    public static final String ACTION_AUTHENTICATE = "AuthService:action:auth";
    public static final String ACTION_SIGNUP = "AuthService:action:signup";

    private static final String EXTRA_CREDENTIALS = "credentials";
    private static final String EXTRA_ACCOUNT = "account";

    public AuthService() {
        super("AuthService");
    }

    // Authenticates existing users
    public static void authenticate(Context c, ApiContract.Credentials credentials) {
        Log.d("authenticate", "authenticating");
        Intent i = new Intent(c, AuthService.class);
        i.setAction(ACTION_AUTHENTICATE);
        i.putExtra(EXTRA_CREDENTIALS, credentials);
        c.startService(i);
    }

    public static void signup(Context c, ApiContract.Account account, ApiContract.Credentials credentials) {
        Log.d("signup", "signing up");
        Intent i = new Intent(c, AuthService.class);
        i.setAction(ACTION_SIGNUP);
        i.putExtra(EXTRA_ACCOUNT, account);
        i.putExtra(EXTRA_CREDENTIALS, credentials);
        c.startService(i);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("onHandleIntent", intent.getAction());
        switch (intent.getAction()) {
            case ACTION_AUTHENTICATE:
                handleAuth(intent);
                break;
            case ACTION_SIGNUP:
                handleSignup(intent);
                break;
        }
    }

    private void handleAuth(Intent intent) {
        Endpoints.Api api = Endpoints.getApi(this);
        ApiContract.Credentials creds = intent.getParcelableExtra(EXTRA_CREDENTIALS);

        try {
            Response<ApiContract.Token> resp = api.auth(creds).execute();
            if (!resp.isSuccessful()) {
                broadcastStatus(ACTION_AUTHENTICATE, false, R.string.error_failed_authenticate);
                return;
            }
            ApiContract.Token t = resp.body();
            Session.setToken(this, t.key, t.expiry);
            broadcastStatus(ACTION_AUTHENTICATE, true);
        } catch (IOException e) {
            e.printStackTrace();
            broadcastStatus(ACTION_AUTHENTICATE, false, R.string.error_failed_authenticate);
        }
    }

    private void handleSignup(Intent intent) {
        Endpoints.Api api = Endpoints.getApi(this);
        ApiContract.Account account = intent.getParcelableExtra(EXTRA_ACCOUNT);
        ApiContract.Credentials credentials = intent.getParcelableExtra(EXTRA_CREDENTIALS);

        ApiContract.SignupBody signupParams = new ApiContract.SignupBody();
        signupParams.account = account;
        signupParams.credentials = credentials;

        ApiContract.ProfileImageBody imgParams = new ApiContract.ProfileImageBody();
        imgParams.url = account.pictureUrl;

        try {
            // create account
            Response<ApiContract.Token> tokenResponse = api.signup(signupParams).execute();
            if (!tokenResponse.isSuccessful()) {
                broadcastStatus(ACTION_SIGNUP, false, R.string.error_failed_signup);
                return;
            }

            ApiContract.Token t = tokenResponse.body();
            Session.setToken(this, t.key, t.expiry);

            // set image
            Response<ApiContract.ProfileImagePayload> imgResponse = api.uploadProfileImage(t.accountKey, imgParams).execute();
            if (!imgResponse.isSuccessful()) {
                broadcastStatus(ACTION_SIGNUP, false, R.string.error_failed_to_save_profile_image);
                return;
            }

            broadcastStatus(ACTION_SIGNUP, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
