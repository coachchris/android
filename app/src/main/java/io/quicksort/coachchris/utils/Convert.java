package io.quicksort.coachchris.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Convert {

    private static SimpleDateFormat sTimestampFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    /**
     * Returns a numeric value that contains the year, month and day in the format `yyyyMMdd`
     */
    public static int toTimestamp(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;  // prevent 0-index month
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return year * 10000 + month * 100 + day;
    }

    public static int toTimestamp(long val) {
        return toTimestamp(new Date(val));
    }

    public static String toFormattedTimestamp(Date d) {
        return sTimestampFormatter.format(d);
    }

    public static String toFormattedTimestamp(long val) {
        return sTimestampFormatter.format(new Date(val));
    }

    public static Date toDateFromTimestamp(String scheduledOn) {
        try {
            return sTimestampFormatter.parse(scheduledOn);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date(0);
        }
    }
}
