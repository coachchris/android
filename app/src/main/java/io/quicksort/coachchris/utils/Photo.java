package io.quicksort.coachchris.utils;

import android.content.Context;

import io.quicksort.coachchris.R;

public class Photo {
    public static String getSizedUrl(Context c, String name, int width, int height) {
        String base = c.getResources().getString(R.string.image_api_url);
        return String.format("%s/images.v1/images?name=%s&w=%d&h=%d", base, name, width, height);
    }
}
