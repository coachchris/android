package io.quicksort.coachchris.utils;

import android.graphics.drawable.Drawable;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

public class Placeholder {
    public static Drawable getDrawable(String name) {
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        int color = generator.getColor(name);
        return TextDrawable.builder().buildRect(String.valueOf(name.charAt(0)), color);
    }
}
