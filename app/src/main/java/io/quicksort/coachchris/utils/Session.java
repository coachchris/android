package io.quicksort.coachchris.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Date;

import io.quicksort.coachchris.models.Account;
import io.realm.Realm;

public class Session {
    private static final String PREF_ACCOUNT_KEY = "pref_account_key";
    private static final String PREF_TOKEN_KEY = "pref_token_key";
    private static final String PREF_TOKEN_EXPIRY = "pref_token_expiry";

    public static String getAccountKey(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(PREF_ACCOUNT_KEY, "");
    }

    public static Account getCurrentUser(Context context, Realm r) {
        return r.where(Account.class).equalTo(Account.ATTR_KEY, Session.getAccountKey(context)).findFirst();
    }

    public static void setAccountKey(Context context, String accountKey) {
        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefs.putString(PREF_ACCOUNT_KEY, accountKey);
        prefs.apply();
    }

    public static void setToken(Context c, String token, Date expiry) {
        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(c).edit();
        prefs.putString(PREF_TOKEN_KEY, token);
        prefs.putLong(PREF_TOKEN_EXPIRY, expiry.getTime());
        prefs.apply();
    }

    public static String getToken(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getString(PREF_TOKEN_KEY, "");
    }

    public static boolean isExpired(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        long expiry = prefs.getLong(PREF_TOKEN_EXPIRY, 0);
        long now = new Date().getTime();

        return expiry < now;
    }
}
