package io.quicksort.coachchris;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.quicksort.coachchris.adapters.FriendsAdapter;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.utils.Session;
import io.realm.Realm;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FriendsFragment extends Fragment {

    private RecyclerView mList;
    private TextView mNoFriends;
    private Realm mRealm;
    private Account currentUser;

    public FriendsFragment() {
        // empty
    }

    public static FriendsFragment newInstance() {
        return new FriendsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_friends, container, false);

        // toolbar
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.friends_toolbar);
        toolbar.setBackgroundResource(R.color.colorPrimary);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle(R.string.activity_friends);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        // menu
        setHasOptionsMenu(true);

        // views
        mNoFriends = (TextView) v.findViewById(android.R.id.empty);
        mList = (RecyclerView) v.findViewById(android.R.id.list);
        mList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST));
        mList.setLayoutManager(new LinearLayoutManager(getContext()));

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        // accounts user is following
        mRealm = Realm.getDefaultInstance();
        currentUser = mRealm.where(Account.class)
                .equalTo(Account.ATTR_KEY, Session.getAccountKey(getContext()))
                .findFirst();


        // bind friends
        mNoFriends.setVisibility(currentUser.following.size() == 0 ? VISIBLE : GONE);
        FriendsAdapter mFriendsAdapter = new FriendsAdapter(getContext(), currentUser.following);
        mList.setAdapter(mFriendsAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.friends_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_friends_search) {
            startActivity(new Intent(getContext(), FriendsSearchActivity.class));
            return true;
        }

        return false;
    }

    // This method may be used again sometime soon
    // private void bindFacebookFriends() {
    //     GraphRequest gr = GraphRequest.newMyFriendsRequest(
    //             AccessToken.getCurrentAccessToken(),
    //             new GraphRequest.GraphJSONArrayCallback() {
    //                 @Override
    //                 public void onCompleted(JSONArray objects, GraphResponse response) {
    //                     final Account currentUser = mRealm.where(Account.class)
    //                             .equalTo(Account.ATTR_KEY, Session.getAccountKey(getContext()))
    //                             .findFirst();

    //                     // existing facebook ids to filter out
    //                     final List<String> followingFacebookIds = new ArrayList<>();
    //                     for (int i = 0; i < currentUser.following.size(); i++) {
    //                         Account a = currentUser.following.get(i);
    //                         if (a.authProviderName.equals("facebook")) {
    //                             followingFacebookIds.add(a.authProviderId);
    //                         }
    //                     }

    //                     // suggested follow ids
    //                     String[] facebookIds = new String[objects.length()];
    //                     for (int i = 0; i < objects.length(); i++) {
    //                         try {
    //                             String fib = objects.getJSONObject(i).getString("id");
    //                             if (!followingFacebookIds.contains(fib)) {
    //                                 facebookIds[i] = fib;
    //                             }
    //                         } catch (JSONException e) {
    //                             e.printStackTrace();
    //                         }
    //                     }

    //                     // fetch accounts matching the facebook ids
    //                     Endpoints.getApi(getActivity())
    //                             .getAccountsByFacebookIds(TextUtils.join(",", facebookIds))
    //                             .enqueue(new Callback<List<Account>>() {
    //                                 @Override
    //                                 public void onResponse(Call<List<Account>> call, Response<List<Account>> response) {
    //                                     if (!response.isSuccessful()) {
    //                                         mNoFriends.setVisibility(VISIBLE);
    //                                         return;
    //                                     }

    //                                     List<Account> friends = response.body();
    //                                     mNoFriends.setVisibility(friends.size() == 0 ? VISIBLE : GONE);
    //                                     mFacebookAdapter = new FriendsSearch(friends);
    //                                     mList.setAdapter(mFacebookAdapter);
    //                                 }

    //                                 @Override
    //                                 public void onFailure(Call<List<Account>> call, Throwable t) {
    //                                     Snackbar.make(getView(), getResources().getString(R.string.error_no_internet), Snackbar.LENGTH_LONG).show();
    //                                 }
    //                             });
    //                 }
    //             });
    //
    //     Bundle params = new Bundle();
    //     params.putString("fields");
    //     gr.setParameters(params);
    //     gr.executeAsync();
    // }
}
