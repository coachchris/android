package io.quicksort.coachchris;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.facebook.FacebookSdk;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static String TAB_INDEX = "tabIndex";
    private FragmentManager mFragmentManager = getSupportFragmentManager();

    private static final int TAB_SCHEDULE_INDEX = 0;
    private static final int TAB_ACTIVITY_INDEX = 1;
    private static final int TAB_PROGRAMS_INDEX = 2;
    private static final int TAB_FRIENDS_INDEX = 3;

    private int mTabIndex = 0;
    private AHBottomNavigation bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());    // must be done first
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            mTabIndex = savedInstanceState.getInt(TAB_INDEX, TAB_SCHEDULE_INDEX);
        }

        initBottomNavigation();
        loadCurrentFragment(mTabIndex);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(TAB_INDEX, mTabIndex);
        super.onSaveInstanceState(outState);
    }

    private void loadCurrentFragment(int index) {
        Fragment f;
        mTabIndex = index;
        switch (index) {
            case TAB_SCHEDULE_INDEX:
                f = TodaysWorkoutFragment.newInstance();
                break;
            case TAB_ACTIVITY_INDEX:
                f = ActivityFragment.newInstance();
                break;
            case TAB_FRIENDS_INDEX:
                f = FriendsFragment.newInstance();
                break;
            case TAB_PROGRAMS_INDEX:
                f = ProgramsFragment.newInstance();
                break;
            default:
                throw new IllegalArgumentException("Invalid tab index");
        }

        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, f)
                .commit();
    }

    @SuppressWarnings("deprecation")
    private void initBottomNavigation() {
        final ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigation.setForceTitlesDisplay(true);

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_today, R.drawable.ic_calendar, R.color.white);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_activity, R.drawable.ic_activity, R.color.white);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_programs, R.drawable.ic_programs, R.color.white);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_friends, R.drawable.ic_friends, R.color.white);

        bottomNavigationItems.add(item1);
        bottomNavigationItems.add(item2);
        bottomNavigationItems.add(item3);
        bottomNavigationItems.add(item4);

        bottomNavigation.addItems(bottomNavigationItems);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            bottomNavigation.setAccentColor(getResources().getColor(R.color.colorAccent, getTheme()));
            bottomNavigation.setInactiveColor(getResources().getColor(R.color.colorBottomNavigationInactive, getTheme()));
            bottomNavigation.setNotificationBackgroundColor(getResources().getColor(R.color.colorBottomNavigationNotification, getTheme()));
        } else {
            bottomNavigation.setAccentColor(getResources().getColor(R.color.colorAccent));
            bottomNavigation.setInactiveColor(getResources().getColor(R.color.colorBottomNavigationInactive));
            bottomNavigation.setNotificationBackgroundColor(getResources().getColor(R.color.colorBottomNavigationNotification));
        }

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (!wasSelected) {
                    loadCurrentFragment(position);
                    bottomNavigation.restoreBottomNavigation(true);
                }
                return true;
            }
        });
    }
}
