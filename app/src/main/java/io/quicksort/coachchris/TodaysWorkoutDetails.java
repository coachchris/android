package io.quicksort.coachchris;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.uncod.android.bypass.Bypass;
import io.quicksort.coachchris.models.Program;
import io.realm.Realm;
import io.realm.RealmChangeListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodaysWorkoutDetails extends Fragment {

    private String programKey;
    private Program program;
    private Realm realm;

    public static Fragment newInstance(String programKey) {
        TodaysWorkoutDetails f = new TodaysWorkoutDetails();
        f.programKey = programKey;
        return f;
    }

    public TodaysWorkoutDetails() {
        // Required empty public constructor
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (!realm.isClosed()) {
            realm.close();
        }

        if (program != null) {
            program.removeChangeListeners();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_todays_workout_details, container, false);
        realm = Realm.getDefaultInstance();
        program = realm.where(Program.class).equalTo(Program.ATTR_KEY, programKey).findFirstAsync();

        program.addChangeListener(new RealmChangeListener<Program>() {
            @Override
            public void onChange(Program program) {
                TextView t = (TextView) v.findViewById(R.id.today_program_details);
                Bypass bypass = new Bypass(getContext());
                CharSequence instructions = bypass.markdownToSpannable(program.instructions);
                t.setText(instructions);
                t.setMovementMethod(LinkMovementMethod.getInstance());

                program.removeChangeListeners();
            }
        });

        return v;
    }
}
