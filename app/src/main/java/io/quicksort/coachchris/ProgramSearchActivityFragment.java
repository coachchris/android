package io.quicksort.coachchris;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.quicksort.coachchris.backend.ProgramService;
import io.quicksort.coachchris.models.Program;
import io.quicksort.coachchris.utils.Photo;
import io.quicksort.coachchris.utils.Placeholder;
import io.realm.Case;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

public class ProgramSearchActivityFragment extends Fragment {

    private RecyclerView programListView;
    private TextView noDataView;
    private Realm mRealm;
    private LocalBroadcastManager mLbm;
    private String mFilter;
    private RealmResults<Program> programs;

    public ProgramSearchActivityFragment() {
        // required
    }

    private BroadcastReceiver mProgramSearchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            View v = getView();
            if (v == null) {
                return;
            }

            boolean success = intent.getBooleanExtra(ProgramService.BROADCAST_SUCCESS, false);
            String msg = intent.getStringExtra(ProgramService.BROADCAST_MESSAGE);

            if (!success) {
                Snackbar.make(v, msg, Snackbar.LENGTH_SHORT).show();
            } else {
                bindPrograms();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLbm = LocalBroadcastManager.getInstance(getContext());
        mRealm = Realm.getDefaultInstance();

        mLbm.registerReceiver(mProgramSearchReceiver, new IntentFilter(ProgramService.ACTION_SEARCH_PROGRAMS));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLbm.unregisterReceiver(mProgramSearchReceiver);
        if (programs != null) {
            programs.removeChangeListeners();
        }
        mRealm.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_program_search, container, false);
        final Context c = getContext();
        setHasOptionsMenu(true);

        programListView = (RecyclerView) v.findViewById(android.R.id.list);
        programListView.setLayoutManager(new LinearLayoutManager(c));
        programListView.setAdapter(null);

        noDataView = (TextView) v.findViewById(android.R.id.empty);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_program_search, menu);

        final MenuItem searchItem = menu.findItem(R.id.menu_item_program_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        // tweak the search text color
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(Color.WHITE);
        searchEditText.setHintTextColor(Color.WHITE);

        // set focus on the search view
        searchView.setIconified(false);

        final Context context = getContext();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                mFilter = query;
                ProgramService.searchPrograms(context, query);

                // Reset SearchView
                searchView.clearFocus();
                searchView.setQuery("", false);
                searchView.setIconified(true);
                searchItem.collapseActionView();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    /**
     * Binds the found programs to the list
     */
    private void bindPrograms() {
        final Context c = getContext();
        programs = mRealm.where(Program.class)
                .contains("tags.value", mFilter, Case.INSENSITIVE)
                .findAllAsync();
        programs.addChangeListener(new RealmChangeListener<RealmResults<Program>>() {
            @Override
            public void onChange(RealmResults<Program> element) {
                if (!programs.isLoaded() || !programs.isValid()) {
                    return;
                }

                ProgramsAdapter adapter = new ProgramsAdapter(c, programs);
                programListView.setAdapter(adapter);

                // Set activity title to search query
                getActivity().setTitle(mFilter);

                if (programs.size() == 0) {
                    noDataView.setVisibility(View.VISIBLE);
                    noDataView.setText(R.string.no_matching_programs_were_found);
                } else {
                    noDataView.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * Binds the found programs
     */
    private class ProgramsAdapter extends RealmRecyclerViewAdapter<Program, ProgramsAdapter.ViewHolder> {

        private int avatarWidth;

        ProgramsAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<Program> data) {
            super(context, data, true);
            avatarWidth = (int) context.getResources().getDimension(R.dimen.list_avatar_diameter);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.program_list_item, parent, false);
            return new ProgramsAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Program p = getItem(position);
            if (p == null) {
                return;
            }

            holder.programKey = p.key;
            holder.name.setText(p.name);
            holder.details.setText(p.description);

            if (!TextUtils.isEmpty(p.photo.name)) {
                String photoUrl = Photo.getSizedUrl(context, p.photo.name, avatarWidth, 0);
                Picasso.with(context)
                        .load(photoUrl)
                        .placeholder(Placeholder.getDrawable(p.name))
                        .into(holder.img);
            } else {
                holder.img.setImageDrawable(Placeholder.getDrawable(p.name));
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img;
            TextView name;
            TextView details;
            String programKey;

            ViewHolder(View v) {
                super(v);

                name = (TextView) v.findViewById(R.id.program_name);
                details = (TextView) v.findViewById(R.id.program_details);
                img = (ImageView) v.findViewById(R.id.program_photo);

                CardView card = (CardView) v.findViewById(R.id.program_card_view);
                card.setPreventCornerOverlap(false);

                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = ProgramDetailsActivity.getIntent(context, programKey);
                        context.startActivity(intent);
                    }
                });
            }
        }
    }
}
