package io.quicksort.coachchris;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;

/**
 * Custom Dialog shown when a program has been completed
 */

public class CompleteProgramDialogFragment extends AppCompatDialogFragment {
    private View.OnClickListener onCompleteClickListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
                .setTitle(R.string.complete_program_title)
                .setMessage(R.string.complete_program_message)
                .setNegativeButton(R.string.complete_program_negative_text, null)
                .setPositiveButton(R.string.complete_program_positive_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onCompleteClickListener.onClick(getView());
                    }
                })
                .create();
    }

    public void setOnCompleteClickListener(View.OnClickListener onCompleteClickListener) {
        this.onCompleteClickListener = onCompleteClickListener;
    }
}
