package io.quicksort.coachchris;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.quicksort.coachchris.backend.AccountService;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.utils.Photo;
import io.quicksort.coachchris.utils.Placeholder;
import io.realm.Case;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

public class FriendsSearchActivityFragment extends Fragment {

    private LocalBroadcastManager mLbm;
    private RecyclerView mList;
    private TextView mNoData;
    private Realm mRealm;

    /**
     * Handler when friend search results are returned
     */
    private BroadcastReceiver mSearchReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final boolean success = intent.getBooleanExtra(AccountService.BROADCAST_SUCCESS, false);
            final String msg = intent.getStringExtra(AccountService.BROADCAST_MESSAGE);
            if (!success) {
                View v = getView();
                if (v != null) {
                    Snackbar.make(v, msg, Snackbar.LENGTH_LONG).show();
                }
            }
        }
    };

    public FriendsSearchActivityFragment() {
        // empty
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRealm = Realm.getDefaultInstance();
        mLbm = LocalBroadcastManager.getInstance(getContext());
        mLbm.registerReceiver(mSearchReceiver, new IntentFilter(AccountService.ACTION_SEARCH_ACCOUNTS));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mLbm.unregisterReceiver(mSearchReceiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_friends_search, container, false);

        mList = (RecyclerView) v.findViewById(android.R.id.list);
        mList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mNoData = (TextView) v.findViewById(android.R.id.empty);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_friends_search, menu);

        // bind the search view
        final MenuItem searchItem = menu.findItem(R.id.menu_friends_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        // tweak the search text color
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(Color.WHITE);
        searchEditText.setHintTextColor(Color.WHITE);

        // set focus on the search view
        searchView.setIconified(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                AccountService.searchAccounts(getContext(), query);

                // close search
                searchView.clearFocus();
                searchView.setQuery("", false);
                searchView.setIconified(true);
                searchItem.collapseActionView();

                // set title to query string
                AppCompatActivity a = (AppCompatActivity) getActivity();
                ActionBar ab = a.getSupportActionBar();
                String queryTitle = getResources().getString(R.string.common_search_results, query);
                if (ab != null) {
                    ab.setTitle(queryTitle);
                }

                // bind any local data now, the listener will update the data if non-local matches are found
                final RealmResults<Account> accounts = mRealm.where(Account.class)
                        .contains(Account.ATTR_NAME, query, Case.INSENSITIVE)
                        .findAllAsync();
                accounts.addChangeListener(new RealmChangeListener<RealmResults<Account>>() {
                    @Override
                    public void onChange(RealmResults<Account> data) {
                        mNoData.setVisibility(data.size() == 0 ? View.VISIBLE : View.GONE);
                        FriendsAdapter a = new FriendsAdapter(getContext(), accounts, true);
                        mList.setAdapter(a);
                    }
                });

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    /**
     * Adapter to bind the list of friends
     */
    private class FriendsAdapter extends RealmRecyclerViewAdapter<Account, FriendsAdapter.ViewHolder> {

        private int imageWidth;

        FriendsAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<Account> data, boolean autoUpdate) {
            super(context, data, autoUpdate);
            imageWidth = (int) getResources().getDimension(R.dimen.list_avatar_diameter);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.friend_list_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Account friend = getItem(position);
            if (friend == null) {
                return;
            }

            String url = Photo.getSizedUrl(getContext(), friend.photo.name, imageWidth, 0);
            holder.accountKey = friend.key;
            holder.userName.setText(friend.name);
            Picasso.with(context)
                    .load(url)
                    .placeholder(Placeholder.getDrawable(friend.firstName))
                    .into(holder.userPhoto);
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            String accountKey;
            ImageView userPhoto;
            TextView userName;

            ViewHolder(View v) {
                super(v);
                v.setOnClickListener(this);
                userPhoto = (ImageView) v.findViewById(R.id.friend_picture);
                userName = (TextView) v.findViewById(R.id.friend_name);
            }

            @Override
            public void onClick(View view) {
                Intent intent = ProfileActivity.getIntent(context, accountKey);
                context.startActivity(intent);
            }
        }
    }
}
