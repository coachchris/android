package io.quicksort.coachchris.backend;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import io.quicksort.coachchris.R;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.models.ActivityFeed;
import io.quicksort.coachchris.models.Person;
import io.quicksort.coachchris.utils.Network;
import io.quicksort.coachchris.utils.Session;
import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;
import retrofit2.Response;

import static io.quicksort.coachchris.backend.Endpoints.getApi;

public class AccountService extends BackendService {

    // actions
    public static final String ACTION_SYNC_ACCOUNT = "accountService:action:sync";
    public static final String ACTION_FETCH_ACCOUNT = "accountService:action:fetch";
    public static final String ACTION_COMPLETE_PROGRAM = "accountService:action:completeProgram";
    public static final String ACTION_FOLLOW = "accountService:action:follow";
    public static final String ACTION_UNFOLLOW = "accountService:action:unfollow";
    public static final String ACTION_SET_WORKOUT_INDEX = "accountService:action:setDayIndex";
    public static final String ACTION_EXTRA_NEW_ACCOUNT_WORKOUT_INDEX = "accountService:action:accountCurrentIndex";
    public static final String ACTION_SEARCH_ACCOUNTS = "accountService:action:searchAccounts";
    public static final String ACTION_FETCH_ACTIVITY_FEED = "accountService:action:activityFeed";

    // Extras
    private static final String ACTION_EXTRA_ACCOUNT_KEY = "accountKey";
    private static final String ACTION_EXTRA_SEARCH_TEXT = "actionExtra:accountFilter";

    Realm realm;

    public AccountService() {
        super("AccountService");
    }

    public static void fetchActivityFeed(Context c) {
        Intent intent = new Intent(c, AccountService.class);
        intent.setAction(ACTION_FETCH_ACTIVITY_FEED);
        c.startService(intent);
    }

    public static void completeProgram(Context context) {
        Intent intent = new Intent(context, AccountService.class);
        intent.setAction(ACTION_COMPLETE_PROGRAM);
        context.startService(intent);
    }

    public static void searchAccounts(Context context, String filter) {
        Intent intent = new Intent(context, AccountService.class);
        intent.setAction(ACTION_SEARCH_ACCOUNTS);
        intent.putExtra(ACTION_EXTRA_SEARCH_TEXT, filter);
        context.startService(intent);
    }

    public static void setWorkoutIndex(Context context, int newWorkoutIndex) {
        Intent intent = new Intent(context, AccountService.class);
        intent.setAction(ACTION_SET_WORKOUT_INDEX);
        intent.putExtra(ACTION_EXTRA_NEW_ACCOUNT_WORKOUT_INDEX, newWorkoutIndex);
        context.startService(intent);
    }

    public static void syncAccount(Context context) {
        Intent intent = new Intent(context, AccountService.class);
        intent.setAction(ACTION_SYNC_ACCOUNT);
        context.startService(intent);
    }

    public static void fetchAccount(Context context, String accountKey) {
        Intent intent = new Intent(context, AccountService.class);
        intent.setAction(ACTION_FETCH_ACCOUNT);
        intent.putExtra(ACTION_EXTRA_ACCOUNT_KEY, accountKey);
        context.startService(intent);
    }

    public static void followAccount(Context c, String accountKey) {
        Intent intent = new Intent(c, AccountService.class);
        intent.setAction(ACTION_FOLLOW);
        intent.putExtra(ACTION_EXTRA_ACCOUNT_KEY, accountKey);
        c.startService(intent);
    }

    public static void unfollowAccount(Context c, String accountKey) {
        Intent intent = new Intent(c, AccountService.class);
        intent.setAction(ACTION_UNFOLLOW);
        intent.putExtra(ACTION_EXTRA_ACCOUNT_KEY, accountKey);
        c.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (!Network.isConnected(this)) {
            broadcastStatus(intent.getAction(), false, R.string.error_no_internet);
            return;
        }

        realm = Realm.getDefaultInstance();

        try {
            switch (intent.getAction()) {
                case ACTION_FETCH_ACTIVITY_FEED:
                    handleActionFetchAccountFeed();
                    break;
                case ACTION_SEARCH_ACCOUNTS:
                    handleActionSearchAccounts(intent);
                    break;
                case ACTION_FETCH_ACCOUNT:
                    handleActionFetchAccount(intent);
                    break;
                case ACTION_SYNC_ACCOUNT:
                    handleActionSyncAccount();
                    break;
                case ACTION_FOLLOW:
                    handleFollowAccount(intent);
                    break;
                case ACTION_UNFOLLOW:
                    handleUnFollowAccount(intent);
                    break;
                case ACTION_SET_WORKOUT_INDEX:
                    handleSetWorkoutIndex(intent);
                    break;
                case ACTION_COMPLETE_PROGRAM:
                    handleCompleteProgram();
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }

    }

    private void handleActionFetchAccountFeed() {
        try {
            Response<List<ActivityFeed>> r = getApi(this).getActivityFeed().execute();
            if (r.isSuccessful()) {
                List<ActivityFeed> feed = r.body();
                try {
                    realm.beginTransaction();
                    realm.insertOrUpdate(feed);
                    realm.commitTransaction();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("AccountService", "handleActionFetchAccountFeed: ", e);
        }
    }

    private void handleActionSearchAccounts(Intent intent) {
        String filter = intent.getStringExtra(ACTION_EXTRA_SEARCH_TEXT);
        boolean isSuccess = false;

        try {
            Response<List<Account>> resp = Endpoints.getApi(this).searchAccounts(filter).execute();
            if (resp.isSuccessful()) {
                final List<Account> accounts = resp.body();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        for (Account account : accounts) {
                            try {
                                realm.insert(account);
                            } catch (RealmPrimaryKeyConstraintException e) {
                                // do nothing
                            }
                        }
                    }
                });
                isSuccess = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        broadcastStatus(ACTION_SEARCH_ACCOUNTS, isSuccess, isSuccess ? 0 : R.string.error_failed_search);
    }

    private void handleCompleteProgram() {
        boolean successful = false;

        try {
            String accountKey = Session.getAccountKey(this);
            Response<Void> r = Endpoints.getApi(this).completeProgram(accountKey).execute();
            successful = r.isSuccessful();
            if (successful) {
                // TODO: something should be updated on the device indicating that the program is complete
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        broadcastStatus(ACTION_COMPLETE_PROGRAM, successful);
    }

    private void handleSetWorkoutIndex(Intent intent) {
        int newIndex = intent.getIntExtra(ACTION_EXTRA_NEW_ACCOUNT_WORKOUT_INDEX, 0);
        boolean success = false;

        try {
            ApiContract.WorkoutIndexBody p = new ApiContract.WorkoutIndexBody();
            p.workout = newIndex;
            Response<Void> r = Endpoints.getApi(this).setWorkoutIndex(p).execute();
            success = r.isSuccessful();
            if (success) {
                Account a = realm.where(Account.class).equalTo(Account.ATTR_KEY, Session.getAccountKey(this)).findFirst();
                realm.beginTransaction();
                a.programLog.dayIndex = newIndex;
                realm.commitTransaction();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        broadcastStatus(ACTION_SET_WORKOUT_INDEX, success);
    }

    private void handleActionFetchAccount(Intent params) throws IOException {
        String accountKey = params.getStringExtra(ACTION_EXTRA_ACCOUNT_KEY);
        Response<Account> r = getApi(this).getAccount(accountKey).execute();
        final Account account = r.body();

        if (r.isSuccessful()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.insertOrUpdate(account);
                }
            });
        }
    }

    private void handleActionSyncAccount() {
        boolean success = false;
        int message = 0;

        try {
            Response<Account> r = Endpoints.getApi(this).syncAccount().execute();
            if (r.isSuccessful()) {
                Account account = r.body();
                realm.beginTransaction();
                {
                    realm.insertOrUpdate(account);
                }
                realm.commitTransaction();
                success = true;
                Session.setAccountKey(this, account.key);
            }
        } catch (RealmPrimaryKeyConstraintException e) {
            message = R.string.error_failed_account_sync;
            realm.cancelTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }

        broadcastStatus(ACTION_SYNC_ACCOUNT, success, message);
    }

    private void handleFollowAccount(Intent intent) throws IOException {
        final String accountKey = intent.getStringExtra(ACTION_EXTRA_ACCOUNT_KEY);
        boolean isSuccess = false;
        String message = "";

        final Context context = this;
        ApiContract.FollowFriendBody body = new ApiContract.FollowFriendBody();
        body.account = accountKey;
        final Response<Person> resp = Endpoints.getApi(this).followAccount(body).execute();
        if (resp.isSuccessful()) {
            final Account currentUser = realm.where(Account.class)
                    .equalTo(Account.ATTR_KEY, Session.getAccountKey(context))
                    .findFirst();

            final Account user = realm.where(Account.class)
                    .equalTo(Account.ATTR_KEY, accountKey)
                    .findFirst();

            realm.beginTransaction();
            currentUser.following.add(resp.body());
            realm.commitTransaction();
            isSuccess = true;
            message = getResources().getString(R.string.follow_success, user.firstName);
        }

        broadcastStatus(ACTION_FOLLOW, isSuccess, message);
    }

    private void handleUnFollowAccount(Intent intent) throws IOException {
        final String accountKey = intent.getStringExtra(ACTION_EXTRA_ACCOUNT_KEY);
        final Context context = this;

        Response<Void> resp = Endpoints.getApi(this).unfollowAccount(accountKey).execute();
        if (resp.isSuccessful()) {
            final Account currentUser = realm.where(Account.class)
                    .equalTo(Account.ATTR_KEY, Session.getAccountKey(context))
                    .findFirst();

            final Account account = realm.where(Account.class)
                    .equalTo(Account.ATTR_KEY, accountKey)
                    .findFirst();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    try {
                        // remove following id
                        for (int i = 0; i < currentUser.following.size(); i++) {
                            if (currentUser.following.get(i).key.equals(accountKey)) {
                                currentUser.following.remove(i);
                                break;
                            }
                        }
                        String message = getResources().getString(R.string.unfollow_success, account.firstName);
                        broadcastStatus(ACTION_UNFOLLOW, true, message);
                    } catch (Exception e) {
                        broadcastStatus(ACTION_UNFOLLOW, false, getResources().getString(R.string.unfollow_error));
                        throw e;
                    }
                }
            });

            return;
        }

        broadcastStatus(ACTION_UNFOLLOW, false, getResources().getString(R.string.unfollow_error));
    }
}
