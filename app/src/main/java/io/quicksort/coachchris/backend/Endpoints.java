package io.quicksort.coachchris.backend;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import io.quicksort.coachchris.BuildConfig;
import io.quicksort.coachchris.R;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.models.ActivityFeed;
import io.quicksort.coachchris.models.Person;
import io.quicksort.coachchris.models.Program;
import io.quicksort.coachchris.models.ProgramLog;
import io.quicksort.coachchris.utils.Convert;
import io.quicksort.coachchris.utils.Session;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public class Endpoints {
    private static final Gson sGson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ").create();
    private static final GsonConverterFactory sGsonFactory = GsonConverterFactory.create(sGson);

    /**
     * Static helpers to obtain api class refs
     */

    public static Api getApi(Context c) {
        return getInstance(c).create(Api.class);
    }

    /**
     * Helpers
     */

    private static Retrofit getInstance(Context c) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addNetworkInterceptor(AuthTokenInterceptor.newInstance(c));

        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(new StethoInterceptor());
        }

        OkHttpClient client = builder.build();

        return new Retrofit.Builder()
                .addConverterFactory(sGsonFactory)
                .client(client)
                .baseUrl(c.getResources().getString(R.string.api_url))
                .build();
    }

    /**
     * Interfaces
     */

    public interface Api {
        // auth
        @POST("/v1/auth")
        Call<ApiContract.Token> auth(@Body ApiContract.Credentials credentials);

        // signup
        @POST("/v1/signup")
        Call<ApiContract.Token> signup(@Body ApiContract.SignupBody data);

        @POST("/v1/attachments")
        Call<ApiContract.ProfileImagePayload> uploadProfileImage(@Query("parent") String accountKey, @Body ApiContract.ProfileImageBody data);

        // accounts
        @GET("/v1/accounts")
        Call<List<Account>> searchAccounts(@Query("filter") String filter);

        @GET("/v1/accounts")
        Call<Account> getAccount(@Query("account") String account);

        @PATCH("/v1/accounts?action=setWorkoutIndex")
        Call<Void> setWorkoutIndex(@Body ApiContract.WorkoutIndexBody body);

        // me
        @GET("/v1/me")
        Call<Account> syncAccount();

        // feed
        @GET("/v1/feed")
        Call<List<ActivityFeed>> getActivityFeed();

        // accountPrograms
        @POST("/v1/accountPrograms")
        Call<ProgramLog> joinProgram(@Body ApiContract.AccountProgramsBody body);

        @PATCH("/v1/accountPrograms?action=complete")
        Call<Void> completeProgram(@Query("account") String account);

        @POST("/v1/friends")
        Call<Person> followAccount(@Body ApiContract.FollowFriendBody body);

        @DELETE("/v1/friends")
        Call<Void> unfollowAccount(@Query("account") String account);

        // programs
        @GET("/v1/programs")
        Call<List<Program>> searchPrograms(@Query("filter") String filter);

        @GET("/v1/programs")
        Call<Program> getProgram(@Query("program") String program);

        @PUT("/v1/programs")
        Call<Program> update(@Query("program") String programKey, @Body Program program);
    }

    private static final class AuthTokenInterceptor implements Interceptor {
        private static final String TAG = "AuthTokenIntercept";
        private Context context;

        private AuthTokenInterceptor() { }

        static AuthTokenInterceptor newInstance(Context c) {
            AuthTokenInterceptor instance = new AuthTokenInterceptor();
            instance.context = c;
            return instance;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request r = chain.request();

            // only add the token if it is already exists
            String token = Session.getToken(this.context);
            if (token == null || token.length() == 0) {
                return chain.proceed(r);
            }

            // add the auth token
            Request authRequest = r.newBuilder().addHeader("Authorization", "token=" + token).build();
            Response resp = chain.proceed(authRequest);

            // save the possibly new auth token
            String newToken = resp.header("new-auth-token", "");
            if (newToken.length() > 0 && !newToken.equals(token)) {
                java.util.Date newTokenExpiry = Convert.toDateFromTimestamp(resp.header("new-auth-token-expiry", ""));
                Session.setToken(this.context, newToken, newTokenExpiry);
            }

            return resp;
        }
    }
}
