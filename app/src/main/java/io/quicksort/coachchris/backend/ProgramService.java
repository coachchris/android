package io.quicksort.coachchris.backend;

import android.content.Context;
import android.content.Intent;

import java.io.IOException;
import java.util.List;

import io.quicksort.coachchris.R;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.models.Program;
import io.quicksort.coachchris.models.ProgramLog;
import io.quicksort.coachchris.utils.Network;
import io.quicksort.coachchris.utils.Session;
import io.realm.Realm;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;
import retrofit2.Response;

public class ProgramService extends BackendService {
    // actions
    public static final String ACTION_SEARCH_PROGRAMS = "ProgramService:CachePrograms";
    public static final String ACTION_JOIN_PROGRAM = "ProgramService:JoinProgram";
    public static final String ACTION_FETCH_PROGRAM = "ProgramService:FetchProgram";

    // action arguments
    private static final String ARG_KEY = "arg:key";
    private static final String ARG_FILTER = "arg:filter";

    private Realm realm;

    public ProgramService() {
        super("ProgramService");
    }

    public static void searchPrograms(Context context, String filter) {
        Intent intent = new Intent(context, ProgramService.class);
        intent.setAction(ACTION_SEARCH_PROGRAMS);
        intent.putExtra(ARG_FILTER, filter);
        context.startService(intent);
    }

    public static void fetchProgram(Context context, String key) {
        Intent intent = new Intent(context, ProgramService.class);
        intent.setAction(ACTION_FETCH_PROGRAM);
        intent.putExtra(ARG_KEY, key);
        context.startService(intent);
    }

    public static void joinProgram(Context c, String key) {
        Intent intent = new Intent(c, ProgramService.class);
        intent.setAction(ACTION_JOIN_PROGRAM);
        intent.putExtra(ARG_KEY, key);
        c.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (!Network.isConnected(this)) {
            broadcastStatus(intent.getAction(), false, R.string.error_no_internet);
            return;
        }

        realm = Realm.getDefaultInstance();

        try {
            switch (intent.getAction()) {
                case ACTION_SEARCH_PROGRAMS:
                    performProgramSearch(intent);
                    break;
                case ACTION_FETCH_PROGRAM:
                    performFetchProgram(intent);
                    break;
                case ACTION_JOIN_PROGRAM:
                    performJoinProgram(intent.getStringExtra(ARG_KEY));
                    break;
            }
        } finally {
            realm.close();
        }
    }

    private void performFetchProgram(Intent intent) {
        String programKey = intent.getStringExtra(ARG_KEY);
        boolean isSuccessful = false;
        int message = 0;

        try {
            Response<Program> response = Endpoints.getApi(this).getProgram(programKey).execute();
            if (response.isSuccessful()) {
                realm.beginTransaction();
                realm.insertOrUpdate(response.body());
                realm.commitTransaction();
                isSuccessful = true;
            }
            broadcastStatus(ACTION_FETCH_PROGRAM, isSuccessful);
        } catch (IOException e) {
            e.printStackTrace();
            message = R.string.error_failed_to_obtain_data;
        }

        broadcastStatus(ACTION_FETCH_PROGRAM, isSuccessful, message);
    }

    private void performJoinProgram(String programKey) {
        String accountKey = Session.getAccountKey(this);
        boolean isSuccessful = false;
        int message = R.string.error_failed_to_perform_update;

        try {
            ApiContract.AccountProgramsBody body = new ApiContract.AccountProgramsBody();
            body.account = accountKey;
            body.program = programKey;
            Response<ProgramLog> resp = Endpoints.getApi(this).joinProgram(body).execute();
            if (resp.isSuccessful()) {
                ProgramLog log = resp.body();
                Account a = realm.where(Account.class).equalTo(Account.ATTR_KEY, accountKey).findFirst();
                Program p = realm.where(Program.class).equalTo(Program.ATTR_KEY, programKey).findFirst();

                realm.beginTransaction();
                {
                    realm.insertOrUpdate(log);
                    a.program = p;
                    a.programLog = realm.where(ProgramLog.class).equalTo(ProgramLog.ATTR_KEY, log.key).findFirst();
                }
                realm.commitTransaction();
                isSuccessful = true;
                message = 0;
            }
        } catch (RealmException e) {
            realm.cancelTransaction();
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        broadcastStatus(ACTION_JOIN_PROGRAM, isSuccessful, message);
    }

    private void performProgramSearch(Intent args) {
        String filter = args.getStringExtra(ARG_FILTER);
        final boolean[] success = {false};
        final int[] message = {R.string.error_failed_to_obtain_data};

        try {
            Response<List<Program>> resp = Endpoints.getApi(this).searchPrograms(filter).execute();
            final List<Program> programs = resp.body();
            if (resp.isSuccessful()) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        for (Program p : programs) {
                            try {
                                // HACK: realm.insert() does not insert all the data (tags)
                                boolean exists = realm.where(Program.class).equalTo(Program.ATTR_KEY, p.key).count() > 0;
                                if (!exists) {
                                    realm.insertOrUpdate(p);
                                }
                            } catch (RealmPrimaryKeyConstraintException e) {
                                // just means the object is already in the database
                            }
                        }
                        success[0] = true;
                        message[0] = 0;
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        broadcastStatus(ACTION_SEARCH_PROGRAMS, success[0], message[0]);
    }

}
