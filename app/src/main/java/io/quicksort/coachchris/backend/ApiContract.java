package io.quicksort.coachchris.backend;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Contains classes that are mapped to the api endpoint payloads
 */
public class ApiContract {

    static final class WorkoutIndexBody {
        public int workout;
    }

    static final class AccountProgramsBody {
        public String account;
        public String program;
    }

    static final class FollowFriendBody {
        public String account;
    }

    // response
    public static final class Token {
        public String key;
        public Date expiry;
        public String accountKey;
    }

    /**
     * Request payload containing the facebook account data used when
     * creating/authenticating with the backend api
     */
    public static final class Account implements Parcelable {
        public String key;
        public String name;
        public String firstName;
        public String lastName;
        public String location;
        public String gender;
        public String locale;
        public String pictureUrl;
        public int timezone;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.key);
            dest.writeString(this.name);
            dest.writeString(this.firstName);
            dest.writeString(this.lastName);
            dest.writeString(this.location);
            dest.writeString(this.gender);
            dest.writeString(this.locale);
            dest.writeString(this.pictureUrl);
            dest.writeInt(this.timezone);
        }

        public Account() {
        }

        protected Account(Parcel in) {
            this.key = in.readString();
            this.name = in.readString();
            this.firstName = in.readString();
            this.lastName = in.readString();
            this.location = in.readString();
            this.gender = in.readString();
            this.locale = in.readString();
            this.pictureUrl = in.readString();
            this.timezone = in.readInt();
        }

        public static final Parcelable.Creator<Account> CREATOR = new Parcelable.Creator<Account>() {
            @Override
            public Account createFromParcel(Parcel source) {
                return new Account(source);
            }

            @Override
            public Account[] newArray(int size) {
                return new Account[size];
            }
        };
    }

    public static final class Credentials implements Parcelable {
        public String providerId;
        public String providerName;
        public String providerToken;

        public String username;
        public String password;

        public Credentials() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.providerId);
            dest.writeString(this.providerName);
            dest.writeString(this.providerToken);
            dest.writeString(this.username);
            dest.writeString(this.password);
        }

        protected Credentials(Parcel in) {
            this.providerId = in.readString();
            this.providerName = in.readString();
            this.providerToken = in.readString();
            this.username = in.readString();
            this.password = in.readString();
        }

        public static final Creator<Credentials> CREATOR = new Creator<Credentials>() {
            @Override
            public Credentials createFromParcel(Parcel source) {
                return new Credentials(source);
            }

            @Override
            public Credentials[] newArray(int size) {
                return new Credentials[size];
            }
        };
    }

    public static final class SignupBody {
        public Credentials credentials;
        public Account account;
    }

    public static final class SignupPayload {
        public Credentials credentials;
        public Account account;
    }

    public static final class ProfileImageBody {
        public String url;
    }

    public static final class ProfileImagePayload {
        public String name;
        public String type;
    }
}
