package io.quicksort.coachchris.backend;

class ApiError {
    private String message;
    private int code;

    ApiError(String msg, int code) {
        this.message = msg;
        this.code = code;
    }
}
