package io.quicksort.coachchris.backend;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Provides common flags for all IntentServices that sync data with the backend.
 * All IntentServices that perform this should extend from this class.
 */

public abstract class BackendService extends IntentService {

    public static final String BROADCAST_SUCCESS = "broadcast:success";
    public static final String BROADCAST_MESSAGE = "broadcast:message";

    public BackendService(String name) {
        super(name);
    }

    protected void broadcastStatus(String action, boolean success) {
        broadcastStatus(action, success, "");
    }

    protected void broadcastStatus(String action, boolean success, int stringId) {
        if (stringId <= 0) {
            broadcastStatus(action, success);
        } else {
            broadcastStatus(action, success, getResources().getString(stringId));
        }
    }

    protected void broadcastStatus(String action, boolean success, String message) {
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        Intent intent = new Intent(action);
        intent.putExtra(BROADCAST_SUCCESS, success);
        intent.putExtra(BROADCAST_MESSAGE, message);

        lbm.sendBroadcast(intent);
    }
}
