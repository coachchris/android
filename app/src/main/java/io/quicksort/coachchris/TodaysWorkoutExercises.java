package io.quicksort.coachchris;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import io.quicksort.coachchris.backend.AccountService;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.models.Exercise;
import io.quicksort.coachchris.models.Program;
import io.quicksort.coachchris.utils.Session;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmRecyclerViewAdapter;

import static io.quicksort.coachchris.utils.Network.isConnected;

public class TodaysWorkoutExercises extends Fragment {

    private Program mProgram;

    private String programKey;
    private Realm realm;
    private Account mAccount;

    public TodaysWorkoutExercises() {
        // Required empty public constructor
    }

    public static Fragment newInstance(String programKey) {
        TodaysWorkoutExercises f = new TodaysWorkoutExercises();
        f.programKey = programKey;
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        mAccount = Session.getCurrentUser(getContext(), realm);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_todays_workout_exercises, container, false);
        final RecyclerView rv = (RecyclerView) v.findViewById(R.id.today_exercises);

        // list
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST);
        rv.addItemDecoration(divider);

        // data
        mProgram = realm.where(Program.class).equalTo(Program.ATTR_KEY, programKey).findFirstAsync();
        mProgram.addChangeListener(new RealmChangeListener<RealmModel>() {
            @Override
            public void onChange(RealmModel m) {
                int index = mAccount.programLog.dayIndex;
                if (index >= mProgram.workouts.size()) {
                    return;
                }

                final RealmList<Exercise> exercises = mProgram.workouts.get(index).exercises;
                ExerciseAdapter adapter = new ExerciseAdapter(getContext(), mProgram, exercises);
                rv.setAdapter(adapter);
            }
        });

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mProgram.removeChangeListeners();

        if (realm != null && !realm.isClosed()) {
            realm.close();
        }
    }

    private void markAsComplete() {
        View v = getView();
        if (v == null) {
            return;
        }

        if (!isConnected(getContext())) {
            Snackbar.make(v, R.string.error_no_internet, Snackbar.LENGTH_LONG).show();
            return;
        }

        int newIndex = mAccount.programLog.dayIndex + 1;
        AccountService.setWorkoutIndex(getContext(), newIndex);
    }

    private class ExerciseAdapter extends RealmRecyclerViewAdapter<Exercise, RecyclerView.ViewHolder> {

        private static final int TYPE_HEADER = 0;
        private static final int TYPE_DATA = 1;
        private static final int TYPE_FOOTER = 2;

        private final boolean mIsLastWorkout;
        private final boolean mIsRestDay;

        ExerciseAdapter(@NonNull Context context, @NonNull Program program, @Nullable OrderedRealmCollection<Exercise> data) {
            super(context, data, true);
            mProgram = program;
            mIsRestDay = data.size() == 0;
            mIsLastWorkout = mProgram.workouts.size() == mAccount.programLog.dayIndex + 1;
        }

        @Override
        public int getItemCount() {
            return super.getItemCount() + 2;
        }

        @Override
        public int getItemViewType(int position) {
            if (isHeader(position)) {
                return TYPE_HEADER;
            }
            if (isFooter(position)) {
                return TYPE_FOOTER;
            }
            return TYPE_DATA;
        }

        private boolean isHeader(int position) {
            return position == 0;
        }

        private boolean isFooter(int position) {
            return getItemCount() - 1 == position;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            LayoutInflater li = LayoutInflater.from(getContext());
            switch (viewType) {
                case TYPE_HEADER:
                    v = li.inflate(R.layout.fragment_todays_workout_header, parent, false);
                    return new HeaderHolder(v);
                case TYPE_FOOTER:
                    v = li.inflate(R.layout.fragment_todays_workout_footer, parent, false);
                    return new FooterHolder(v);
                case TYPE_DATA:
                    v = LayoutInflater.from(getContext()).inflate(R.layout.fragment_exercise, parent, false);
                    return new ExerciseAdapter.ViewHolder(v);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (getItemViewType(position) == TYPE_HEADER) {
                bindHeader((HeaderHolder) holder);
                return;
            }

            if (getItemViewType(position) == TYPE_FOOTER) {
                // no additional binding needed for footer
                return;
            }

            bindData((ViewHolder) holder, getItem(position - 1));
        }

        private void bindHeader(HeaderHolder h) {
            h.setName(mProgram.name);
            h.setDetails(mProgram.description);
            h.setDayOf(mAccount.programLog.dayIndex, mProgram.workouts.size());
        }

        private void bindData(final ViewHolder h, final Exercise e) {
            Context c = getContext();
            h.name.setText(e.details);

            if (!TextUtils.isEmpty(e.videoId)) {
                h.btn.setImageDrawable(ContextCompat.getDrawable(c, R.drawable.ic_videocam_accent));
                h.btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("vnd.youtube://" + e.videoId));
                        startActivity(intent);
                    }
                });
            }

//            if (e.reference.hasImage()) {
//                h.btn.setImageDrawable(ContextCompat.getDrawable(c, R.drawable.ic_description_accent));
//                h.btn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setDataAndType(Uri.parse(e.reference.url), "image/*");
//                        startActivity(intent);
//                    }
//                });
//            }
        }

        class HeaderHolder extends RecyclerView.ViewHolder {
            TextView name;
            TextView dayOf;
            TextView details;

            HeaderHolder(View v) {
                super(v);
                name = (TextView) v.findViewById(R.id.today_program_name);
                dayOf = (TextView) v.findViewById(R.id.today_program_day_of);
                details = (TextView) v.findViewById(R.id.today_program_details);
            }

            void setName(String val) {
                name.setText(val);
            }

            void setDayOf(int dayIndex, int workoutCount) {
                dayOf.setText(getResources().getString(R.string.program_day_x_of_y, dayIndex + 1, workoutCount));
            }

            void setDetails(String val) {
                details.setText(val);
            }
        }

        class FooterHolder extends RecyclerView.ViewHolder {
            FooterHolder(View v) {
                super(v);

                final Button b = (Button) v.findViewById(R.id.today_mark_as_completed);
                final int txtId = mIsLastWorkout ? R.string.todays_workout_complete_program : R.string.todays_workout_mark_as_completed;
                final TextView restDay = (TextView) v.findViewById(R.id.today_rest_day);

                if (mIsRestDay) {
                    restDay.setVisibility(View.VISIBLE);
                }

                b.setText(getResources().getString(txtId));
                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!mIsLastWorkout) {
                            markAsComplete();
                            return;
                        }

                        CompleteProgramDialogFragment f = new CompleteProgramDialogFragment();
                        f.setOnCompleteClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // TODO: There should be a lbr listener here in case this fails...
                                AccountService.completeProgram(getContext());

                                // TODO: update layout here to instruct them to choose a new program
                                // start the intro activity here...
                            }
                        });
                        f.show(getChildFragmentManager(), "completeProgram");
                    }
                };

                b.setOnClickListener(listener);
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView name;
            ImageView btn;

            ViewHolder(View v) {
                super(v);

                name = (TextView) v.findViewById(R.id.exercise_details);
                btn = (ImageButton) v.findViewById(R.id.exercise_demo_button);
            }
        }
    }
}
