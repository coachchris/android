package io.quicksort.coachchris.views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.quicksort.coachchris.R;


public class HorizontalImageTextViewHolder extends RecyclerView.ViewHolder {
    private ImageView mImage;
    private TextView mText;

    public HorizontalImageTextViewHolder(View v) {
        super(v);
        mImage = (ImageView) v.findViewById(R.id.image_1);
        mText = (TextView) v.findViewById(R.id.text_1);
    }

    public static View getView(LayoutInflater inflater, ViewGroup viewGroup) {
        return inflater.inflate(R.layout.horizontal_image_text, viewGroup, false);
    }

    public void setImageDrawable(String url) {
        Picasso.with(itemView.getContext()).load(url).into(mImage);
    }

    public void setText(String value) {
        mText.setText(value);
    }
}
