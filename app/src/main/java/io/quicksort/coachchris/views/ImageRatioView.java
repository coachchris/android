package io.quicksort.coachchris.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import io.quicksort.coachchris.R;

public class ImageRatioView extends ImageView {

    TypedArray attrs;

    public ImageRatioView(Context context) {
        super(context, null, 0);
    }

    public ImageRatioView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        this.attrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ImageRatioView, 0, 0);
    }

    public ImageRatioView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.attrs = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ImageRatioView, 0, 0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        float verticalRatio = attrs.getFloat(R.styleable.ImageRatioView_verticalRatio, 0f);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = (int)(width * verticalRatio);
        int ratioHeightSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, ratioHeightSpec);
    }
}
