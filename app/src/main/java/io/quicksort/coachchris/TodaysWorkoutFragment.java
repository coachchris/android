package io.quicksort.coachchris;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import io.quicksort.coachchris.backend.AccountService;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.utils.Photo;
import io.quicksort.coachchris.utils.Session;
import io.realm.Realm;
import io.realm.RealmChangeListener;

import static io.quicksort.coachchris.utils.Network.isConnected;

public class TodaysWorkoutFragment extends Fragment {

    private Realm realm;
    private Account account;

    public static TodaysWorkoutFragment newInstance() {
        return new TodaysWorkoutFragment();
    }

    public TodaysWorkoutFragment() {
        // Required empty public constructor
    }

    // handle updates
    BroadcastReceiver incrementIndexBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            View v = getView();
            if (v == null) {
                return;
            }

            boolean success = intent.getBooleanExtra(AccountService.BROADCAST_SUCCESS, false);
            if (success) {
                Snackbar.make(v, R.string.todays_workout_mark_complete_success, Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(v, R.string.todays_workout_mark_complete_error, Snackbar.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.todays_workout, menu);
    }

    private void gotoPreviousWorkout() {
        View v = getView();
        if (v == null) {
            return;
        }

        if (!isConnected(getContext())) {
            Snackbar.make(v, R.string.error_no_internet, Snackbar.LENGTH_LONG).show();
            return;
        }

        AccountService.setWorkoutIndex(getContext(), account.programLog.dayIndex - 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_todays_workout_go_to_previous:
                gotoPreviousWorkout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (realm != null && !realm.isClosed()) {
            realm.close();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_todays_workout, container, false);

        setHasOptionsMenu(true);

        realm = Realm.getDefaultInstance();

        // toolbar
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.today_toolbar);
        AppCompatActivity a = (AppCompatActivity) getActivity();
        toolbar.setTitle("");  // blank out the overlayed title
        a.setSupportActionBar(toolbar);

        // account
        String accountKey = Session.getAccountKey(getContext());
        account = realm.where(Account.class)
                .equalTo(Account.ATTR_KEY, accountKey)
                .findFirstAsync();

        account.addChangeListener(new RealmChangeListener<Account>() {
            @Override
            public void onChange(Account account) {
                if (!account.isLoaded() || !account.isValid()) {
                    return;
                }

                // bg photo
                ImageView backdrop = (ImageView) v.findViewById(R.id.today_backdrop);
                int width = (int) getResources().getDimension(R.dimen.program_details_image_width);
                String photoUrl = Photo.getSizedUrl(getContext(), account.program.photo.name, width, 0);
                Picasso.with(getContext())
                        .load(photoUrl)
                        .into(backdrop);

                // exercises
                bindPager(v);

                account.removeChangeListener(this);
            }
        });


        LocalBroadcastManager
                .getInstance(getContext())
                .registerReceiver(incrementIndexBroadcastReceiver,
                        new IntentFilter(AccountService.ACTION_SET_WORKOUT_INDEX));

        return v;
    }

    private void bindPager(View v) {
        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) v.findViewById(R.id.today_pager);

        pager.setAdapter(new TodaysWorkoutPagerAdapter(getChildFragmentManager()));
        tabLayout.setupWithViewPager(pager);
    }

    private class TodaysWorkoutPagerAdapter extends FragmentStatePagerAdapter {

        private static final int TAB_EXERCISES = 0;
        private static final int TAB_DETAILS = 1;

        TodaysWorkoutPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case TAB_EXERCISES:
                    return getResources().getString(R.string.todays_workout_tab_exercises);
                case TAB_DETAILS:
                    return getResources().getString(R.string.todays_workout_tab_details);
            }
            return null;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case TAB_EXERCISES:
                    return TodaysWorkoutExercises.newInstance(account.program.key);
                case TAB_DETAILS:
                    return TodaysWorkoutDetails.newInstance(account.program.key);
            }

            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
