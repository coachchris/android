package io.quicksort.coachchris;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

abstract class GroupedRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private class Group {
        int count;
        int typeId;

        Group(int count, int typeId) {
            this.count = count;
            this.typeId = typeId;
        }
    }

    private List<Group> mGroups = new ArrayList<>();

    void addGroup(int typeId, int count) {
        mGroups.add(new Group(count, typeId));
    }

    private int getGroupIndex(int position) {
        int offset = 0;
        for (int i = 0; i < mGroups.size(); i++) {
            offset += mGroups.get(i).count;
            if (position < offset) {
                return i;
            }
        }
        throw new RuntimeException("Group item type found: " + position);
    }

    int getRelativeIndex(int position) {
        int groupIndex = getGroupIndex(position);
        int offset = 0;
        for (int i = 0; i < groupIndex; i++) {
            offset += mGroups.get(i).count;
        }
        return position - offset;
    }

    @Override
    public int getItemCount() {
        int count = 0;
        for (Group g : mGroups) {
            count += g.count;
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        int offset = 0;

        for (int i = 0; i < mGroups.size(); i++) {
            offset += mGroups.get(i).count;
            if (position < offset) {
                return mGroups.get(i).typeId;
            }
        }

        throw new RuntimeException("Group item type found: " + position);
    }
}
