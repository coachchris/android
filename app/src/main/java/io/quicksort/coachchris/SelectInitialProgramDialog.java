package io.quicksort.coachchris;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

public class SelectInitialProgramDialog extends AppCompatDialogFragment {

    public static final String ARG_PROGRAM_NAME = "arg:programName";
    public static final String ARG_PROGRAM_KEY = "arg:programKey";

    private OnSelectInitialProgram mListener;

    public interface OnSelectInitialProgram {
        void onSelectInitialProgram(String programKey);
    }

    public void addOnSelectInitialProgramListener(OnSelectInitialProgram l) {
        mListener = l;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (mListener == null) {
            throw new RuntimeException("OnSelectInitalProgram listener must be set");
        }

        Bundle args = getArguments();
        if (args == null) {
            throw new RuntimeException("Bundle args required");
        }

        final String programName = args.getString(ARG_PROGRAM_NAME);
        final String programKey = args.getString(ARG_PROGRAM_KEY);

        return new AlertDialog.Builder(getContext())
                .setTitle(programName)
                .setMessage(R.string.intro_confirmation_message)
                .setNegativeButton(R.string.common_cancel, null)
                .setPositiveButton(R.string.into_start_program, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onSelectInitialProgram(programKey);
                    }
                })
                .create();
    }
}
