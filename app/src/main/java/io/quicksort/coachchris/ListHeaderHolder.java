package io.quicksort.coachchris;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Reusable Viewholder used to display a section title between lists
 */

class ListHeaderHolder extends RecyclerView.ViewHolder {
    private TextView mLabel;

    ListHeaderHolder(View v) {
        super(v);
        mLabel = (TextView) v.findViewById(android.R.id.text1);
    }

    static View getView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.list_header, parent, false);
    }

    void setHeader(int resId) {
        mLabel.setText(resId);
    }
}
