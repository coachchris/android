package io.quicksort.coachchris;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroFragment extends Fragment {

    private OnFragmentListener onFragmentListener;

    public interface OnFragmentListener {
        void findProgram();
    }

    public IntroFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_intro, container, false);

        Button b = (Button) v.findViewById(R.id.intro_find_program);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFragmentListener.findProgram();
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            onFragmentListener = (OnFragmentListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException("IntroFragment requires an OnFragmentListener");
        }
    }
}
