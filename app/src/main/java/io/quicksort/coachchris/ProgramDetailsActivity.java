package io.quicksort.coachchris;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class ProgramDetailsActivity extends AppCompatActivity {

    public static final String ARG_KEY = "key";

    public static Intent getIntent(Context c, String programKey) {
        Intent intent = new Intent(c, ProgramDetailsActivity.class);
        intent.putExtra(ARG_KEY, programKey);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_details);

        String programKey = getIntent().getStringExtra(ARG_KEY);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, ProgramDetailsFragment.getInstance(programKey))
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
