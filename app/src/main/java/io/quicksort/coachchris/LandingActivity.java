package io.quicksort.coachchris;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Arrays;

import io.quicksort.coachchris.backend.AccountService;
import io.quicksort.coachchris.backend.ApiContract;
import io.quicksort.coachchris.facebook.FacebookMePayload;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.services.AuthService;
import io.quicksort.coachchris.utils.Session;
import io.realm.Realm;

import static io.quicksort.coachchris.utils.Network.isConnected;

public class LandingActivity extends AppCompatActivity {

    private Button mLoginButton;
    private CallbackManager mCallbackManager;

    /**
     * Handle the facebook's auth api response
     */
    private final FacebookCallback mFbCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            final AccessToken token = loginResult.getAccessToken();
            GraphRequest gr = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    FacebookMePayload p = new Gson().fromJson(response.getRawResponse(), FacebookMePayload.class);

                    ApiContract.Credentials c = new ApiContract.Credentials();
                    c.providerName = "facebook";
                    c.providerToken = token.getToken();
                    c.providerId = p.id;

                    ApiContract.Account a = new ApiContract.Account();
                    a.name = p.name;
                    a.firstName = p.firstName;
                    a.lastName = p.lastName;
                    a.timezone = p.timezone;
                    a.location = p.location;
                    a.locale = p.locale;
                    a.gender = p.gender;
                    a.pictureUrl = p.picture.data.url;

                    authenticate(a, c);
                }
            });

            Bundle params = new Bundle();
            params.putString("fields", "name,id,first_name,last_name,email,gender,location,locale,timezone,picture.width(800)");
            gr.setParameters(params);
            gr.executeAsync();
        }

        @Override
        public void onCancel() {
            hideLoadingSpinner();
        }

        @Override
        public void onError(FacebookException error) {
            Toast.makeText(getApplicationContext(), getString(R.string.error_failed_to_signin), Toast.LENGTH_LONG).show();
            hideLoadingSpinner();
        }
    };

    private void authenticate(final ApiContract.Account account, final ApiContract.Credentials credentials) {
        LocalBroadcastManager.getInstance(LandingActivity.this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean ok = intent.getBooleanExtra(AuthService.BROADCAST_SUCCESS, false);
                if (!ok) {
                    signup(account, credentials);
                    return;
                }
                syncData();
            }
        }, new IntentFilter(AuthService.ACTION_AUTHENTICATE));

        AuthService.authenticate(LandingActivity.this, credentials);
    }

    private void signup(ApiContract.Account account, ApiContract.Credentials credentials) {
        LocalBroadcastManager.getInstance(LandingActivity.this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean ok = intent.getBooleanExtra(AuthService.BROADCAST_SUCCESS, false);
                if (!ok) {
                    String err = intent.getStringExtra(AuthService.BROADCAST_MESSAGE);
                    Snackbar.make(findViewById(R.id.landing_root), err, 5000)
                            .show();
                    hideLoadingSpinner();
                    return;
                }
                syncData();
            }
        }, new IntentFilter(AuthService.ACTION_SIGNUP));

        AuthService.signup(LandingActivity.this, account, credentials);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());    // must be done first

        setContentView(R.layout.activity_landing);

        // init facebook
        mCallbackManager = CallbackManager.Factory.create();
        mLoginButton = (Button) findViewById(R.id.facebook_auth);
        LoginManager.getInstance().registerCallback(mCallbackManager, mFbCallback);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LandingActivity.this, Arrays.asList("public_profile", "user_friends"));
                showLoadingSpinner();
            }
        });

        if (!isConnected(this) && hasAccountKey()) {
            Intent intent = new Intent(this, MainActivity.class);
            // toast allows the message to still be shown in the next activity
            Toast.makeText(this, R.string.error_no_internet, Toast.LENGTH_LONG).show();
            startActivity(intent);
            return;
        }

        // listen for status of account sync
        IntentFilter filter = new IntentFilter(AccountService.ACTION_SYNC_ACCOUNT);
        AccountSyncStatusReceiver receiver = new AccountSyncStatusReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);

        if (!hasAccountKey()) {
            mLoginButton.setVisibility(View.VISIBLE);
            return;
        }

        // take action based on whether the user is logged in or not
        if (Session.isExpired(this) || AccessToken.getCurrentAccessToken().isExpired()) {
            mLoginButton.setVisibility(View.VISIBLE);
        } else {
            showLoadingSpinner();
            syncData();
        }
    }

    private void showLoadingSpinner() {
        View v = findViewById(R.id.loading);
        v.setVisibility(View.VISIBLE);
        mLoginButton.setVisibility(View.GONE);
    }

    private void hideLoadingSpinner() {
        View v = findViewById(R.id.loading);
        v.setVisibility(View.GONE);
        mLoginButton.setVisibility(View.VISIBLE);
    }

    private void syncData() {
        AccountService.syncAccount(this);
        AccountService.fetchActivityFeed(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Handle whether or not the user was logged in or not
     */
    private class AccountSyncStatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean success = intent.getBooleanExtra(AccountService.BROADCAST_SUCCESS, false);
            String accountKey = Session.getAccountKey(context);

            // failed api and no account key exists
            if (!success && TextUtils.isEmpty(accountKey)) {
                Snackbar.make(
                        findViewById(R.id.landing_root),
                        R.string.error_failed_account_sync_with_no_account_key,
                        5000
                ).show();
                mLoginButton.setVisibility(View.VISIBLE);
                return;
            }

            // failed to connect to api, but an account key exists
            if (!success) {
                Toast.makeText(context, R.string.error_failed_account_sync, Toast.LENGTH_LONG).show();
            }

            // send new users to program selection activity
            Realm r = Realm.getDefaultInstance();
            Account a = r.where(Account.class).equalTo(Account.ATTR_KEY, accountKey).findFirst();
            boolean inProgram = a.program != null;
            if (!inProgram) {
                Intent activity = new Intent(LandingActivity.this, IntroActivity.class);
                activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(activity);
                return;
            }

            //
            Intent activity = new Intent(LandingActivity.this, MainActivity.class);
            activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(activity);
        }
    }

    private boolean hasAccountKey() {
        return !TextUtils.isEmpty(Session.getAccountKey(this));
    }
}
