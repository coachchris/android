package io.quicksort.coachchris.facebook;

import com.google.gson.annotations.SerializedName;

/**
 * Payload for facebook API `GET /accounts/me`
 */

public class FacebookMePayload {

    public String id;
    public String gender;
    public String locale;
    public String location;
    public String name;
    public int timezone;
    public Picture picture;

    @SerializedName("first_name")
    public String firstName;

    @SerializedName("last_name")
    public String lastName;

    // not included in json, here to for `toApiPayload` method
    public String token;

    public class PictureDetails {
        @SerializedName("is_silhouette")
        public boolean isSilhouette;
        public String url;
    }

    public class Picture {
        public PictureDetails data;
    }
}
