package io.quicksort.coachchris;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

import io.quicksort.coachchris.backend.ProgramService;
import io.quicksort.coachchris.models.Account;
import io.quicksort.coachchris.models.Person;
import io.quicksort.coachchris.models.Program;
import io.quicksort.coachchris.models.StringValue;
import io.quicksort.coachchris.utils.Photo;
import io.quicksort.coachchris.utils.Session;
import io.quicksort.coachchris.views.HorizontalImageTextViewHolder;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;

/**
 * Program Details
 */
public class ProgramDetailsFragment extends Fragment {

    private static final String ARG_KEY = "key";

    private static final NumberFormat nf = NumberFormat.getIntegerInstance(Locale.getDefault());
    private static final NumberFormat pf = NumberFormat.getPercentInstance(Locale.getDefault());

    private Realm mRealm;
    private LocalBroadcastManager mLbm;
    private Account mCurrentAccount;
    private Program mProgram;
    private Toolbar mToolbar;
    private ImageView mProgramImage;

    /**
     * Receiver called once the program has been fetched from the api
     */
    private BroadcastReceiver mProgramReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isSuccess = intent.getBooleanExtra(ProgramService.BROADCAST_SUCCESS, false);
            String message = intent.getStringExtra(ProgramService.BROADCAST_MESSAGE);
            if (!isSuccess) {
                View v = getView();
                if (v == null) {
                    Log.d("onReceive", "getView is null");
                    return;
                }
                Snackbar.make(v, message, Snackbar.LENGTH_SHORT).show();
            }
        }
    };
    /**
     * Shows the appropriate dialog and binds required actions depending on if
     * this is your first program or you are starting a new program.
     */
    private View.OnClickListener toggleProgram = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (isDefaultMode()) {
                showToggleProgramDialog();
            } else if (isIntroMode()) {
                showIntroDialog();
            }
        }

        private void showIntroDialog() {
            Bundle args = new Bundle();
            args.putString(SelectInitialProgramDialog.ARG_PROGRAM_KEY, mProgram.key);
            args.putString(SelectInitialProgramDialog.ARG_PROGRAM_NAME, mProgram.name);

            SelectInitialProgramDialog d = new SelectInitialProgramDialog();
            d.setArguments(args);
            d.addOnSelectInitialProgramListener(new SelectInitialProgramDialog.OnSelectInitialProgram() {
                @Override
                public void onSelectInitialProgram(String programKey) {
                    final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getContext());
                    lbm.registerReceiver(new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            Intent a = new Intent(getContext(), MainActivity.class);
                            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(a);

                            lbm.unregisterReceiver(this);
                        }
                    }, new IntentFilter(ProgramService.ACTION_JOIN_PROGRAM));

                    ProgramService.joinProgram(getContext(), programKey);
                }
            });
            d.show(getChildFragmentManager(), "selectInitialProgramDialog");
        }

        private void showToggleProgramDialog() {
            Bundle args = new Bundle();
            args.putString(ToggleExistingProgramDialog.ARG_PROGRAM_KEY, getProgramKey());

            ToggleExistingProgramDialog d = new ToggleExistingProgramDialog();
            d.setArguments(args);
            d.show(getChildFragmentManager(), "toggleProgramDialog");
        }
    };

    /**
     * Helper method to create fragment bundled up with the required data
     *
     * @param programKey Unique key for the program
     * @return ProgramDetailsFragment
     */
    public static ProgramDetailsFragment getInstance(String programKey) {
        ProgramDetailsFragment f = new ProgramDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, programKey);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();
        mCurrentAccount = mRealm.where(Account.class).equalTo(Account.ATTR_KEY,
                Session.getAccountKey(getContext())).findFirst();

        mLbm = LocalBroadcastManager.getInstance(getContext());
        mLbm.registerReceiver(mProgramReceiver, new IntentFilter(ProgramService.ACTION_FETCH_PROGRAM));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_program_details, container, false);

        // toolbar
        mToolbar = (Toolbar) v.findViewById(R.id.programdetails_toolbar);
        AppCompatActivity a = (AppCompatActivity) getActivity();
        a.setSupportActionBar(mToolbar);
        ActionBar actionBar = a.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mProgramImage = (ImageView) v.findViewById(R.id.programdetails_photo);

        mProgram = mRealm.where(Program.class).equalTo(Program.ATTR_KEY, getProgramKey()).findFirstAsync();
        mProgram.addChangeListener(new RealmChangeListener<RealmModel>() {
            @Override
            public void onChange(RealmModel m) {
                View v = getView();
                if (v == null) {
                    return;
                }

                mToolbar.setTitle(mProgram.name);

                float width = getResources().getDimension(R.dimen.program_details_image_width);
                String url = Photo.getSizedUrl(getContext(), mProgram.photo.name, (int) width, 0);
                Picasso.with(getContext()).load(url).into(mProgramImage);

                RecyclerView list = (RecyclerView) v.findViewById(android.R.id.list);
                list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                list.setAdapter(new ProgramDetailsAdapter(mProgram));
            }
        });

        // fetch the account
        ProgramService.fetchProgram(getContext(), getProgramKey());

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mLbm.unregisterReceiver(mProgramReceiver);
        mCurrentAccount.removeChangeListeners();
    }

    /**
     * Binds the data within the local database to the UI.
     * This method should only be called within the broadcast receiver
     */
    private void bindData() {
    }

    private boolean isDefaultMode() {
        return mCurrentAccount.inProgram();
    }

    private boolean isIntroMode() {
        return !mCurrentAccount.inProgram();
    }

    private String getProgramKey() {
        return getArguments().getString(ARG_KEY);
    }

    private class ProgramDetailsAdapter extends GroupedRecyclerViewAdapter {

        private static final int TYPE_HEADER = 0;
        private static final int TYPE_PARTICIPANTS_HEADER = 1;
        private static final int TYPE_PARTICIPANTS = 2;

        private Program mProgram;
        private int avatarWidth;

        ProgramDetailsAdapter(Program p) {
            mProgram = p;

            // groups
            addGroup(TYPE_HEADER, 1);
            if (mProgram.knownParticipants != null && mProgram.knownParticipants.size() > 0) {
                addGroup(TYPE_PARTICIPANTS_HEADER, 1);
                addGroup(TYPE_PARTICIPANTS, mProgram.knownParticipants.size());
            }

            avatarWidth = (int) getContext().getResources().getDimension(R.dimen.list_avatar_diameter);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View v;

            switch (viewType) {
                case TYPE_HEADER:
                    v = inflater.inflate(R.layout.program_details_header, parent, false);
                    return new ProgramDetailsHeaderViewHolder(v);
                case TYPE_PARTICIPANTS_HEADER:
                    v = ListHeaderHolder.getView(inflater, parent);
                    return new ListHeaderHolder(v);
                case TYPE_PARTICIPANTS:
                    v = HorizontalImageTextViewHolder.getView(inflater, parent);
                    return new HorizontalImageTextViewHolder(v);
            }

            throw new RuntimeException("onCreateViewHolder: Invalid viewType");
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            int relPosition = getRelativeIndex(position);
            int type = getItemViewType(position);

            switch (type) {
                case TYPE_PARTICIPANTS_HEADER:
                    bindParticipantsHeader(holder);
                    break;
                case TYPE_PARTICIPANTS:
                    bindParticipant(holder, relPosition);
                    break;
            }
        }

        private void bindParticipant(RecyclerView.ViewHolder holder, int position) {
            HorizontalImageTextViewHolder h = (HorizontalImageTextViewHolder) holder;

            Person p = mProgram.knownParticipants.get(position);
            String url = Photo.getSizedUrl(getContext(), p.photo.name, avatarWidth, 0);
            h.setImageDrawable(url);
            h.setText(p.name);
        }

        private void bindParticipantsHeader(RecyclerView.ViewHolder holder) {
            ListHeaderHolder header = (ListHeaderHolder) holder;
            header.setHeader(R.string.program_details_people_you_know);
        }

        private class ProgramDetailsHeaderViewHolder extends RecyclerView.ViewHolder {
            private final Button toggleProgramJoin;
            private final TextView totalParticipantCount;
            private final TextView description;
            private final TextView level;
            private final LinearLayout physicalRequirements;
            private final LinearLayout equipmentRequirements;

            ProgramDetailsHeaderViewHolder(View v) {
                super(v);
                Context c = v.getContext();
                Resources r = getResources();

                physicalRequirements = (LinearLayout) v.findViewById(R.id.program_details_physical_requirements);
                if (mProgram.physicalRequirements.size() > 0) {
                    physicalRequirements.setVisibility(View.VISIBLE);
                    for (StringValue str : mProgram.physicalRequirements) {
                        TextView d = new TextView(c);
                        d.setText(r.getString(R.string.bullet_point, str.value));
                        physicalRequirements.addView(d);
                    }
                }

                equipmentRequirements = (LinearLayout) v.findViewById(R.id.program_details_equipment_requirements);
                if (mProgram.equipmentRequirements.size() > 0) {
                    equipmentRequirements.setVisibility(View.VISIBLE);
                    for (StringValue str : mProgram.equipmentRequirements) {
                        TextView d = new TextView(c);
                        d.setText(r.getString(R.string.bullet_point, str.value));
                        equipmentRequirements.addView(d);
                    }
                }

                // toggleStatus
                toggleProgramJoin = (Button) v.findViewById(R.id.program_details_toggle);
                toggleProgramJoin.setOnClickListener(toggleProgram);
                toggleProgramJoin.setVisibility(isIntroMode() ? View.VISIBLE : View.GONE);

                // listen for any later changes to the account's program
                mCurrentAccount.addChangeListener(new RealmChangeListener<Account>() {
                    @Override
                    public void onChange(Account account) {
                        if (account == null || !account.isLoaded() || !account.isValid()) {
                            return;
                        }

                        if (isIntroMode()) {
                            toggleProgramJoin.setVisibility(View.VISIBLE);
                            return;
                        }

                        boolean showButton = !mProgram.key.equals(getProgramKey());
                        toggleProgramJoin.setVisibility(showButton ? View.VISIBLE : View.GONE);
                    }
                });

                totalParticipantCount = (TextView) v.findViewById(R.id.program_details_total_participant_count);
                totalParticipantCount.setText(nf.format(mProgram.totalParticipantCount));

                description = (TextView) v.findViewById(R.id.program_details_description);
                description.setText(mProgram.description);

                int levelGroup = mProgram.level / 10;
                int levelItem = mProgram.level % 10 + 1;
                level = (TextView) v.findViewById(R.id.program_details_level);
                level.setText(getResources().getString(R.string.program_details_level, levelItem));
                switch (levelGroup) {
                    case 0:
                        level.setBackgroundResource(R.drawable.green_pill);
                        break;
                    case 1:
                        level.setBackgroundResource(R.drawable.blue_pill);
                        break;
                    case 2:
                        level.setBackgroundResource(R.drawable.red_pill);
                        break;
                }
            }
        }
    }
}
