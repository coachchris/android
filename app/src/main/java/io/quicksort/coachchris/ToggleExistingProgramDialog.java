package io.quicksort.coachchris;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import io.quicksort.coachchris.backend.ProgramService;

public class ToggleExistingProgramDialog extends AppCompatDialogFragment {

    public static final String ARG_PROGRAM_KEY = "arg:programKey";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        final String programKey = args.getString(ARG_PROGRAM_KEY);

        return new AlertDialog.Builder(getContext())
                .setTitle(R.string.common_confirmation)
                .setMessage(R.string.program_join_confirmation)
                .setPositiveButton(R.string.common_join, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final Context c = getContext();
                        final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(c);
                        lbm.registerReceiver(new BroadcastReceiver() {
                            @Override
                            public void onReceive(Context context, Intent intent) {
                                if (getView() != null) {
                                    Snackbar.make(getView(), R.string.program_join_success, Snackbar.LENGTH_SHORT).show();
                                }
                                lbm.unregisterReceiver(this);
                            }
                        }, new IntentFilter(ProgramService.ACTION_JOIN_PROGRAM));

                        ProgramService.joinProgram(c, programKey);
                    }
                })
                .setNegativeButton(R.string.common_cancel, null)
                .create();
    }
}
