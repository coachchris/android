package io.quicksort.coachchris;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * UI Fragment that contains the views for the Programs tab
 *
 * TODO: fetch trending programs here, right now no data is bound on this page
 */
public class ProgramsFragment extends Fragment {

    public ProgramsFragment() {
        // Required empty public constructor
    }

    public static ProgramsFragment newInstance() {
        return new ProgramsFragment();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.programs_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.programs_menu_search) {
            startActivity(new Intent(getContext(), ProgramSearchActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_programs, container, false);
        getActivity().setTitle(R.string.tab_programs);
        setHasOptionsMenu(true);

        // toolbar
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.programs_toolbar);
        toolbar.setBackgroundResource(R.color.colorPrimary);
        toolbar.setTitleTextColor(Color.WHITE);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        TextView noDataView = (TextView) v.findViewById(android.R.id.empty);
        noDataView.setText(R.string.tmp_popular_programs_coming_soon);

        return v;
    }
}
