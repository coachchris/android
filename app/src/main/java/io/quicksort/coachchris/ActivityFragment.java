package io.quicksort.coachchris;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.Date;

import io.quicksort.coachchris.models.ActivityFeed;
import io.quicksort.coachchris.utils.Photo;
import io.quicksort.coachchris.utils.Placeholder;
import io.quicksort.coachchris.utils.Session;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Contains the activity list for the account and all the people they are following
 */
public class ActivityFragment extends Fragment {

    private Realm realm;
    private RecyclerView mActivityList;
    private TextView mNoData;

    public ActivityFragment() {
        // Required empty public constructor
    }

    public static ActivityFragment newInstance() {
        return new ActivityFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_activity, container, false);

        getActivity().setTitle(R.string.tab_activity);
        mActivityList = (RecyclerView) v.findViewById(android.R.id.list);
        mActivityList.setLayoutManager(new LinearLayoutManager(getContext()));
        mActivityList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST));
        mNoData = (TextView) v.findViewById(android.R.id.empty);

        Toolbar toolbar = (Toolbar) v.findViewById(R.id.activity_toolbar);
        toolbar.setBackgroundResource(R.color.colorPrimary);
        toolbar.setTitleTextColor(Color.WHITE);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        realm = Realm.getDefaultInstance();
        bindData();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (realm != null) {
            realm.close();
        }
    }

    /**
     * Bind the activity list
     */
    private void bindData() {
        RealmResults<ActivityFeed> activity = realm.where(ActivityFeed.class).findAll();
        if (activity.size() > 0) {
            RealmResults<ActivityFeed> sorted = activity.sort("timestamp", Sort.DESCENDING);
            ActivityAdapter adapter = new ActivityAdapter(getContext(), sorted);
            mActivityList.setAdapter(adapter);
            mNoData.setVisibility(View.GONE);
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Activity binding adapter
     */
    private class ActivityAdapter extends RealmRecyclerViewAdapter<ActivityFeed, ActivityAdapter.ActivityViewHolder> {

        private CharSequence[] mActions;
        private String mMeKey;
        private final long mNow;
        private int avatarWidth;

        ActivityAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<ActivityFeed> data) {
            super(context, data, true);

            mActions = context.getResources().getTextArray(R.array.activity_actions);
            mMeKey = Session.getAccountKey(context);
            mNow = new Date().getTime();
            avatarWidth = (int) context.getResources().getDimension(R.dimen.list_avatar_diameter);
        }

        @Override
        public ActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.activity_list_item, parent, false);
            return new ActivityViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ActivityViewHolder holder, int position) {
            OrderedRealmCollection<ActivityFeed> data = getData();
            if (data == null) {
                return;
            }

            ActivityFeed a = data.get(position);
            CharSequence template = mActions[a.code];
            String you = getResources().getString(R.string.You);
            String accountName = a.account.key.equals(mMeKey) ? you : a.account.name;

            String action;
            if (a.link != null) {
                action = String.format(template.toString(), accountName, a.link.name);
            } else {
                action = String.format(template.toString(), accountName);
            }

            Spanned actionFmt;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                actionFmt = Html.fromHtml(action,Html.FROM_HTML_MODE_LEGACY);
            } else {
                //noinspection deprecation
                actionFmt = Html.fromHtml(action);
            }
            holder.Action.setText(actionFmt);

            // timestamp
            long timestamp = a.timestamp.getTime();
            CharSequence relTime = DateUtils.getRelativeTimeSpanString(timestamp, mNow, DateUtils.DAY_IN_MILLIS);
            holder.Timestamp.setText(relTime);

            // image
            if (TextUtils.isEmpty(a.account.photo.name)) {
                holder.ProfilePhoto.setImageDrawable(Placeholder.getDrawable(a.account.name));
            } else {
                String url = Photo.getSizedUrl(getContext(), a.account.photo.name, avatarWidth, 0);
                Picasso.with(getContext())
                        .load(url)
                        .placeholder(Placeholder.getDrawable(a.account.name))
                        .into(holder.ProfilePhoto);
            }
        }

        class ActivityViewHolder extends RecyclerView.ViewHolder {

            private CircularImageView ProfilePhoto;
            private TextView Action;
            private TextView Timestamp;

            ActivityViewHolder(View v) {
                super(v);
                ProfilePhoto = (CircularImageView) v.findViewById(R.id.activity_profile_photo);
                Action = (TextView) v.findViewById(R.id.activity_action);
                Timestamp = (TextView) v.findViewById(R.id.activity_timestamp);
            }
        }
    }
}
