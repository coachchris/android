package io.quicksort.coachchris;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class ProfileActivity extends AppCompatActivity {

    private static final String ACCOUNT_KEY = "key";

    public static Intent getIntent(Context c, String accountKey) {
        Intent intent = new Intent(c, ProfileActivity.class);
        intent.putExtra(ACCOUNT_KEY, accountKey);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        String accountKey = getIntent().getStringExtra(ACCOUNT_KEY);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container_foo, ProfileFragment.newInstance(accountKey))
                .commit();
    }

    /**
     * Allow toolbar back button click to go back
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
